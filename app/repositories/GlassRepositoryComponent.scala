package repositories

import models.cup.Glass

/**
 * Created by gbmetzner on 10/08/14.
 */
trait GlassRepositoryComponent {

  def glassRepository: GlassRepository

  trait GlassRepository extends Repository[Glass]

}
