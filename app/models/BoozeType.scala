package models.cocktype

import java.util.UUID

import org.joda.time.DateTime
import utils.converters._

import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gbmetzner on 20/09/14.
 */
case class BoozeType(id: Option[UUID],
                     typeId: UUID,
                     cocktailId: UUID,
                     createdAt: Option[DateTime] = None,
                     updatedAt: Option[DateTime] = None)

class BoozeTypeTable(tag: Tag) extends Table[BoozeType](tag, "boozetype") {
  def id = column[Option[UUID]]("id", O.PrimaryKey)

  def typeId = column[UUID]("typeid", O.NotNull)

  def cocktailId = column[UUID]("boozeid", O.NotNull)

  def createdAt = column[Option[DateTime]]("createdat", O.NotNull)

  def updatedAt = column[Option[DateTime]]("updatedat", O.NotNull)

  def * = (id, typeId, cocktailId, createdAt, updatedAt) <>(BoozeType.tupled, BoozeType.unapply)
}
