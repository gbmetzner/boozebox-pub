package auth

import java.util.UUID

import controllers.admin.routes
import jp.t2v.lab.play2.auth._
import models.admin.BoozeUser
import play.api.mvc.Results._
import play.api.mvc._
import services.BoozeUserServiceComponent

import scala.concurrent.{ExecutionContext, Future}
import scala.reflect.{ClassTag, classTag}

/**
 * Created by gbmetzner on 06/08/14.
 */
trait BoozeBoxAuthConfig extends AuthConfig {
  requires: BoozeUserServiceComponent =>
  /**
   * A type that is used to identify a user.
   * `String`, `Int`, `Long` and so on.
   */
  type Id = UUID

  /**
   * A type that represents a user in your application.
   * `User`, `Account` and so on.
   */
  type User = BoozeUser

  /**
   * A type that is defined by every action for authorization.
   * This sample uses the following trait:
   *
   * sealed trait Permission
   * case object Administrator extends Permission
   * case object NormalUser extends Permission
   */
  type Authority = Nothing

  /**
   * A `ClassTag` is used to retrieve an id from the Cache API.
   * Use something like this:
   */
  val idTag: ClassTag[Id] = classTag[Id]

  /**
   * The session timeout in seconds
   */
  val sessionTimeoutInSeconds: Int = 3600

  /**
   * The key for the URL accessed
   */
  val accessedURI = "accessed_uri"

  /**
   * The URL for home page
   */
  val homeURL = routes.DashboardController.dashboard().url

  /**
   * The URL for login page
   */
  val loginURL = routes.LoginController.login().url

  /**
   * A function that returns a `User` object from an `Id`.
   * You can alter the procedure to suit your application.
   */
  def resolveUser(id: Id)(implicit ctx: ExecutionContext): Future[Option[User]] =
    Future.successful(boozeUserService findById id)


  /**
   * Where to redirect the user after a successful login.
   */
  def loginSucceeded(request: RequestHeader)(implicit ctx: ExecutionContext): Future[Result] =
    Future.successful {
      (request.session.get(accessedURI) match {
        case Some(url) => Redirect(url)
        case None => Redirect(homeURL)
      }).withSession(request.session - accessedURI)
    }

  /**
   * Where to redirect the user after logging out
   */
  def logoutSucceeded(request: RequestHeader)(implicit ctx: ExecutionContext): Future[Result] =
    Future.successful(Redirect(loginURL))

  /**
   * If the user is not logged in and tries to access a protected resource then redirct them as follows:
   */
  def authenticationFailed(request: RequestHeader)(implicit ctx: ExecutionContext): Future[Result] =
    Future.successful(Redirect(loginURL).withSession(accessedURI -> request.uri))

  /**
   * If authorization failed (usually incorrect password) redirect the user as follows:
   */
  def authorizationFailed(request: RequestHeader)(implicit ctx: ExecutionContext): Future[Result] =
    Future.successful(Forbidden("no permission"))

  /**
   * A function that determines what `Authority` a user has.
   * You should alter this procedure to suit your application.
   */
  def authorize(user: User, authority: Authority)(implicit ctx: ExecutionContext): Future[Boolean] = Future.successful {
    //    (user.permission, authority) match {
    //      case (Administrator, _) => true
    //      case (NormalUser, NormalUser) => true
    //      case _ => false
    //    }
    true
  }

  /**
   * Whether use the secure option or not use it in the cookie.
   * However default is false, I strongly recommend using true in a production.
   */
  override lazy val cookieSecureOption: Boolean = play.api.Play.isProd(play.api.Play.current)

}
