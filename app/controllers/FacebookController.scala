package controllers

import play.api.mvc.{Action, Controller}
import utils.AppConfig
import utils.plugins.Social

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * Created by gbmetzner on 21/06/14.
 */
object FacebookController extends Controller {


  def login = Action {

    val to = s"""${AppConfig.facebookLoginUrl}
              ?client_id=${AppConfig.facebookClientId}
              &redirect_uri=${AppConfig.facebookRedirectUrl}
              &scope=publish_actions"""

    Redirect(to)
  }

  def logout = Action {
    implicit request =>
      Redirect("/").withNewSession
  }

  def callback = Action.async {
    implicit request =>

      request.getQueryString("code") match {
        case Some(code) => Social.login(code).map { access_token =>
          Redirect("/").withSession("access_token" -> access_token)
        }
        case None => Future {
          Redirect("/")
        }
      }
  }
}


