package utils

import java.sql.Timestamp

import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, LocalDate}

import scala.slick.ast.BaseTypedType
import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gbmetzner on 06/07/14.
 */
package object converters {

  import play.api.libs.json._

  private val pattern = "MM/dd/yyyy"

  implicit val dateFormat = Format[LocalDate](Reads.jodaLocalDateReads(pattern), Writes.jodaLocalDateWrites(pattern))

  implicit val dateTime: BaseTypedType[DateTime] =
    MappedColumnType.base[DateTime, Timestamp](
      dt => new Timestamp(dt.getMillis),
      ts => new DateTime(ts.getTime))


  implicit val fmtFullDateTime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss:SSS")

}
