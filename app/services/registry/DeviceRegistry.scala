package services.registry

import repositories.impl.DeviceRepositoryComponentImpl
import services.impl.{AndroidMessengerComponentImpl, DeviceServiceComponentImpl}

/**
 * Created by gbmetzner on 23/11/14.
 */
trait DeviceRegistry extends DeviceServiceComponentImpl with DeviceRepositoryComponentImpl with AndroidMessengerComponentImpl
