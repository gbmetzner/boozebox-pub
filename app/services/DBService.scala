package services

import play.api.Play.current
import play.api.db._

import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gbmetzner on 17/08/14.
 */
trait DBService {
  protected[this] val db = Database.forDataSource(DB.getDataSource("postgres"))
}
