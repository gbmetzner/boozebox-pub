$(document).ready(function () {

    $.getJSON("/admin/cup/all", function (data) {
        $.each(data, function (index, value) {
            $('#cups').append($("<option />").val(value.id).text(value.name + " - " + value.size));
        });
    });

    $('#cocktailForm').bootstrapValidator().on('success.form.bv', function (e) {

        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        var data = JSON.stringify({
                idCup: $('#cups').val(),
                deviceCode: parseInt($('#devcode').val()),
                name: $('#name').val(),
                color: $('#color').val(),
                rgb: $('#rgb').val(),
                pdvCode: parseInt($('#pdvcode').val()),
                author: $('#author').val()
             });

        $.ajax({
            type: 'POST',
            data: data,
            url: $form.attr('action'),
            dataType: 'json',
            headers: { 'Content-Type': 'application/json' },
            success: function (data) {
                $("#dialog").children().empty();
                $("#dialog").children().append(data.message);
                $("#dialog").dialog({
                    modal: true,
                    closeOnEscape: false,
                    dialogClass: 'noclose',
                    buttons: {
                        Ok: function () {
                            window.location.href = '/admin/cocktail/list';
                        }
                    }
                });
            },
            error: function (xhr, status, error) {
                $("#dialog").children().empty();
                $("#dialog").children().append(status.message);
                $("#dialog").dialog({
                    modal: true,
                    closeOnEscape: false,
                    dialogClass: 'noclose',
                    buttons: {
                        Ok: function () {
                            $("#dialog").dialog("close");
                        }
                    }
                });
            }
        });
    });

});