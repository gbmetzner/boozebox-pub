package services

import models.admin.BoozeUser

/**
 * Created by gbmetzner on 2/26/15.
 */
trait BoozeUserServiceComponent {
  def boozeUserService: BoozeUserService

  trait BoozeUserService extends ServiceComponent[BoozeUser] {
    def authenticate(username: String, password: String): Option[BoozeUser]
  }

}
