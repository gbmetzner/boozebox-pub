package services.registry

import repositories.impl.BoozeUserRepositoryComponentImpl
import services.impl.BoozeUserServiceComponentImpl

/**
 * Created by gbmetzner on 2/26/15.
 */
trait BoozeUserRegistry extends BoozeUserServiceComponentImpl with BoozeUserRepositoryComponentImpl
