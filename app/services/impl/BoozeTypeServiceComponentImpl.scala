package services.impl

import java.util.UUID

import models.cocktype.BoozeType
import repositories.BoozeTypeRepositoryComponent
import services.{BoozeTypeServiceComponent, DBService}
import utils.paginate.Page

/**
 * Created by gbmetzner on 23/09/14.
 */
trait BoozeTypeServiceComponentImpl extends BoozeTypeServiceComponent {
  self: BoozeTypeRepositoryComponent =>

  override def boozeTypeService: BoozeTypeService = new BoozeTypeServiceImpl

  class BoozeTypeServiceImpl extends BoozeTypeService with DBService {
    override def persist(boozeType: BoozeType): Unit = db.withTransaction {
      implicit session =>
        boozeType.id match {
          case Some(_) => boozeTypeRepository update boozeType
          case None => boozeTypeRepository save boozeType
        }
    }

    override def findById(id: UUID): Option[BoozeType] = db.withSession {
      implicit session =>
        boozeTypeRepository findById id
    }

    override def delete(id: UUID): Unit = db.withTransaction {
      implicit session =>
        boozeTypeRepository delete id
    }

    override def list(page: Int, length: Int, query: Option[String]): Page[BoozeType] = db.withSession {
      implicit session =>
        boozeTypeRepository list(page, length, query)
    }

    override def listAll: List[BoozeType] = db.withSession {
      implicit session =>
        boozeTypeRepository listAll()
    }
  }

}
