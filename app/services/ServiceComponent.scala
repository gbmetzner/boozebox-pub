package services

import java.util.UUID

import com.typesafe.scalalogging.{LazyLogging, Logger}
import org.slf4j.LoggerFactory
import utils.paginate.Page

/**
 * Created by gbmetzner on 06/11/14.
 */
trait ServiceComponent[T] extends LazyLogging with DBService {

  override protected lazy val logger: Logger = Logger(LoggerFactory.getLogger("services"))

  def persist(t: T): Unit

  def list(page: Int, length: Int, query: Option[String]): Page[T]

  def listAll: List[T]

  def findById(id: UUID): Option[T]

  def delete(id: UUID): Unit
}
