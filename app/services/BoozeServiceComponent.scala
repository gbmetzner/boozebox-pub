package services

import models.booze.Booze

/**
 * Created by gbmetzner on 24/08/14.
 */
trait BoozeServiceComponent {

  def boozeService: BoozeService

  trait BoozeService extends ServiceComponent[Booze] with DBService

}
