package models

import java.util.UUID

import org.joda.time.DateTime
import play.api.libs.json.JsValue
import utils.uuid._
import scala.language.postfixOps
import scala.slick.driver.PostgresDriver.simple._
import scala.slick.lifted.ProvenShape
import utils.converters._

/**
 * Created by gbmetzner on 29/07/14.
 */
case class Order(id: Option[UUID],
                 orderId: Long,
                 owner: String,
                 boozeCode: Int,
                 boozeName: String,
                 voucher: String,
                 inputTime: Option[DateTime],
                 startTime: DateTime,
                 finishTime: DateTime,
                 machine: String)

class OrderTable(tag: Tag) extends Table[Order](tag, "order") {

  def id = column[Option[UUID]]("id", O.PrimaryKey, O.NotNull)

  def orderId = column[Long]("orderid", O.NotNull)

  def owner = column[String]("owner", O.NotNull)

  def boozeCode = column[Int]("boozecode", O.NotNull)

  def boozeName = column[String]("boozename", O.NotNull)

  def voucher = column[String]("voucher", O.NotNull)

  def inputTime = column[Option[DateTime]]("inputtime", O.NotNull)

  def startTime = column[DateTime]("starttime", O.NotNull)

  def finishTime = column[DateTime]("finishtime", O.NotNull)

  def machine = column[String]("machine", O.NotNull)

  override def * : ProvenShape[Order] = (id, orderId, owner, boozeCode, boozeName,
    voucher, inputTime, startTime, finishTime, machine) <>(Order.tupled, Order.unapply)
}