package repositories

import models.ingredient.Ingredient

/**
 * Created by gbmetzner on 17/08/14.
 */
trait IngredientRepositoryComponent {
  def ingredientRepository: IngredientRepository

  trait IngredientRepository extends Repository[Ingredient]

}
