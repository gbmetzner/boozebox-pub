package utils

import java.util.UUID

/**
 * Created by gbmetzner on 07/11/14.
 */
package object uuid {
  def uuid: UUID = UUID.randomUUID
}
