package services.impl

import java.util.UUID

import models.booze.Booze
import repositories.BoozeRepositoryComponent
import services.BoozeServiceComponent
import utils.paginate.Page

/**
 * Created by gbmetzner on 24/08/14.
 */
trait BoozeServiceComponentImpl extends BoozeServiceComponent {
  self: BoozeRepositoryComponent =>

  override def boozeService: BoozeService = new BoozeServiceImpl

  class BoozeServiceImpl extends BoozeService {

    override def persist(booze: Booze): Unit = db.withTransaction {
      implicit session =>
        booze.id match {
          case Some(id) => boozeRepository update booze
          case None => boozeRepository save booze
        }
    }

    override def findById(id: UUID): Option[Booze] = db.withSession {
      implicit session =>
        boozeRepository findById id
    }

    override def delete(id: UUID): Unit = db.withTransaction {
      implicit session =>
        boozeRepository delete id
    }

    override def list(page: Int, length: Int, query: Option[String]): Page[Booze] = db.withSession {
      implicit session =>
        boozeRepository list(page, 10)
    }

    override def listAll: List[Booze] = db.withSession {
      implicit session =>
        boozeRepository listAll()
    }
  }

}
