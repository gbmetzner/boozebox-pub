$(document).ready(function () {

    $.getJSON("/admin/cocktail/all", function (data) {
        $.each(data, function (index, value) {
            $('#cocktails').append($("<option />").val(value.id).text(value.name));
        });
    });

    $.getJSON("/admin/drinktype/all", function (data) {
        $.each(data, function (index, value) {
            $('#types').append($("<option />").val(value.id).text(value.description));
        });
    });

    $('#cocktypeForm').bootstrapValidator().on('success.form.bv', function (e) {

        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        var data = JSON.stringify({
            cocktailId: $('#cocktails').val(),
            typeId: $('#types').val()
        });

        alert($form.attr('action'))

        $.ajax({
            type: 'POST',
            data: data,
            url: $form.attr('action'),
            dataType: 'json',
            headers: { 'Content-Type': 'application/json' },
            success: function (data) {
                $("#dialog").children().empty();
                $("#dialog").children().append(data.message);
                $("#dialog").dialog({
                    modal: true,
                    closeOnEscape: false,
                    dialogClass: 'noclose',
                    buttons: {
                        Ok: function () {
                            window.location.href = '/admin/cocktype/list';
                        }
                    }
                });
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                $("#dialog").children().empty();
                $("#dialog").children().append(err.message);
                $("#dialog").dialog({
                    modal: true,
                    closeOnEscape: false,
                    dialogClass: 'noclose',
                    buttons: {
                        Ok: function () {
                            $("#dialog").dialog("close");
                        }
                    }
                });
            }
        });
    });

});