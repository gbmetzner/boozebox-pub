package services.registry

import repositories.impl.OrderRepositoryComponentImpl
import services.impl.{AndroidMessengerComponentImpl, OrderServiceComponentImpl}

/**
 * Created by gbmetzner on 07/02/15.
 */
trait OrderRegistry extends OrderServiceComponentImpl with AndroidMessengerComponentImpl with BoozeRegistry with OrderRepositoryComponentImpl
