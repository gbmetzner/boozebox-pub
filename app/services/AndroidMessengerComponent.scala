package services

/**
 * Created by gbmetzner on 29/11/14.
 */
trait AndroidMessengerComponent {

  def androidMessenger: AndroidMessenger

  trait AndroidMessenger {
    def sendMessage(codeDevice: String, data: (String, String)*): Boolean
  }

}
