$(document).ready(function () {

    var uploader = $("#image").uploadFile({
        url: '/admin/ingredient/upload',
        fileName: 'image',
        allowedTypes: "png,gif,jpg,jpeg",
        multiple: false,
        maxFileCount: 1,
        autoSubmit: false,
        dragDrop: false,
        showFileCounter: false
    });

    $('#ingredientForm').bootstrapValidator().on('success.form.bv', function (e) {

        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        //var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data


        uploader.startUpload();

        var data = JSON.stringify({
            ingredient: $('#ingredient').val(),
            density: parseFloat($('#density').val()),
            image: $('.ajax-file-upload-filename').text()
        });

        $.ajax({
            type: 'POST',
            data: data,
            url: $form.attr('action'),
            dataType: 'json',
            headers: { 'Content-Type': 'application/json' },
            success: function (data) {
                $("#dialog").children().empty();
                $("#dialog").children().append(data.message);
                $("#dialog").dialog({
                    modal: true,
                    closeOnEscape: false,
                    dialogClass: 'noclose',
                    buttons: {
                        Ok: function () {
                            window.location.href = '/admin/ingredient/list';
                        }
                    }
                });
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                $("#dialog").children().empty();
                $("#dialog").children().append(err.message);
                $("#dialog").dialog({
                    modal: true,
                    closeOnEscape: false,
                    dialogClass: 'noclose',
                    buttons: {
                        Ok: function () {
                            $("#dialog").dialog("close");
                        }
                    }
                });
            }
        });
    });
});