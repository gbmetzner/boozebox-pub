package models.dto

/**
 * Created by gbmetzner on 23/02/15.
 */
case class RankedBooze(quantity: Int, cocktailId: Int, cocktailName: String, cocktailType: String)