package controllers.admin

import java.io.File
import java.util.UUID

import models.cup.Glass
import play.api.libs.json.{JsError, Json}
import play.api.mvc.{Action, BodyParsers}
import services.registry.GlassRegistry
import services.{GlassServiceComponent, UploadServiceComponent}
import utils.AppConfig._
import views.html

/**
 * Created by gbmetzner on 10/08/14.
 */
object GlassController extends GlassController with GlassRegistry

trait GlassController extends BaseController {
  self: GlassServiceComponent with UploadServiceComponent =>

  import utils.parsers.glassFormat

  def form = Action {
    Ok(html.admin.glass.add())
  }

  def save = Action(BodyParsers.parse.json) {
    implicit request =>

      request.body.validate[Glass].fold(
        errors => BadRequest(Json.obj("status" -> "400", "message" -> JsError.toFlatJson(errors))),
        glass => {
          glassService.persist(glass)
          Ok(Json.obj("status" -> "OK", "message" -> s"Glass: ${glass.name} saved."))
        }
      )
  }

  def update(id: UUID) = Action(BodyParsers.parse.json) {
    implicit request =>

      request.body.validate[Glass].fold(
        errors => {
          BadRequest(Json.obj("status" -> "400", "message" -> JsError.toFlatJson(errors)))
        },
        glass => {
          glassService persist glass
          Ok(Json.obj("status" -> "OK", "message" -> s"Glass: ${glass.name} updated."))
        }
      )
  }

  def upload = Action(parse.multipartFormData) {
    implicit request =>

      request.body.file("image").map {
        image =>

          val filename = image.filename
          val file = new File(s"/tmp/$filename")

          image.ref.moveTo(file)

          uploadService upload(s3Bucket, s"$s3CupUploadKey${filename}", file)

          file.delete

          Ok("Ok")
      }.getOrElse {
        NotFound
      }
  }

  /**
   * Find the Glass
   *
   * @param id Current id of the Glass to be updated
   */
  def edit(id: UUID) = Action {
    implicit request =>
      glassService.findById(id) match {
        case Some(cup) => Ok(html.admin.glass.edit(cup))
        case None => NotFound
      }
  }

  /**
   * Display the page list of Glass.
   */
  def listPage = Action {
    implicit request =>
      Ok(html.admin.glass.list())
  }

  /**
   * Display the list of Glass by page.
   */
  def pagination = Action {
    implicit request =>

      val (draw, start, length, query) = parseQueryStringPagination(request.queryString)

      logger info (query.toString)

      val glasses = glassService list(start, length, query)

      val glassesJson = Json.toJson(glasses.items)

      Ok(Json.obj("draw" -> draw, "recordsTotal" -> glasses.total, "recordsFiltered" -> glasses.total, "data" -> glassesJson))
  }

  /**
   * Delete the Glass
   *
   * @param id Current id of the Glass to be deleted.
   */
  def delete(id: UUID) = Action {
    implicit request =>
      glassService delete id
      Ok(Json.obj("message" -> "Glass was deleted."))
  }

  /**
   * List all Glass
   */
  def all = Action {
    Ok(Json.toJson(glassService listAll))
  }

}
