package services

import models.cocktype.BoozeType

/**
 * Created by gbmetzner on 23/09/14.
 */
trait BoozeTypeServiceComponent {

  def boozeTypeService: BoozeTypeService

  trait BoozeTypeService extends ServiceComponent[BoozeType] with DBService

}
