package repositories.impl

import java.util.UUID

import models.device.{Device, DeviceTable}
import org.joda.time.DateTime
import repositories.DeviceRepositoryComponent
import utils.paginate.Page
import utils.uuid._

import scala.slick.driver.PostgresDriver.simple._
import scala.slick.lifted.TableQuery

/**
 * Created by gbmetzner on 22/11/14.
 */
trait DeviceRepositoryComponentImpl extends DeviceRepositoryComponent {

  override def deviceRepository: DeviceRepository = new DeviceRepositoryImpl

  class DeviceRepositoryImpl extends DeviceRepository {

    private val tableQuery = TableQuery[DeviceTable]

    override def save(device: Device)(implicit s: Session): Unit = {
      val now = Some(DateTime.now)
      tableQuery += device.copy(id = Some(uuid), createdAt = now, updatedAt = now)
    }

    override def update(device: Device)(implicit s: Session): Unit = {
      tableQuery.filter(_.id === device.id)
        .map(d => (d.code, d.androidId, d.description, d.updatedAt))
        .update((device.code, device.androidId, device.description, Some(DateTime.now)))
    }

    override def findById(id: UUID)(implicit s: Session): Option[Device] = tableQuery filter (_.id === id) firstOption

    override def delete(id: UUID)(implicit s: Session): Unit = tableQuery filter (_.id === id) delete

    override def list(page: Int, length: Int, query: Option[String])(implicit s: Session): Page[Device] = {

      val filtered = query match {
        case Some(query) => tableQuery.filter(_.description.toLowerCase like s"%${query.toLowerCase}%")
        case None => tableQuery
      }

      val total = filtered.length.run

      Page(filtered.drop(page).take(length).list, page, page, total)
    }

    override def listAll()(implicit s: Session): List[Device] = tableQuery list

    override def findByCode(code: String)(implicit session: Session): Option[Device] = tableQuery filter (_.code === code) firstOption
  }

}
