package models.dto

import java.util.UUID

/**
 * Created by gustavo on 5/27/14.
 */
case class WebOrder(name: String, voucher: Option[String], cocktailId: UUID, postOnFB: Boolean = false)
