package services.registry

import repositories.impl.GlassRepositoryComponentImpl
import services.impl.{UploadServiceComponentImpl, GlassServiceComponentImpl}

/**
 * Created by gbmetzner on 10/08/14.
 */
trait GlassRegistry extends GlassServiceComponentImpl with GlassRepositoryComponentImpl with UploadServiceComponentImpl
