package repositories.impl

import java.util.UUID

import models.dto.RankedBooze
import models.{Order, OrderTable}
import org.joda.time.{DateTimeConstants, DateTime}
import repositories.OrderRepositoryComponent
import utils.paginate.Page
import com.github.tototoshi.slick.PostgresJodaSupport._
import scala.slick.driver.PostgresDriver.simple._
import scala.slick.jdbc.{GetResult, StaticQuery}
import DateTimeConstants._

/**
 * Created by gbmetzner on 10/02/15.
 */
trait OrderRepositoryComponentImpl extends OrderRepositoryComponent {

  override def orderRepository: OrderRepository = new OrderRepositoryImpl

  class OrderRepositoryImpl extends OrderRepository {

    private val tableQuery = TableQuery[OrderTable]

    override def save(order: Order)(implicit session: Session): Unit = tableQuery += order

    override def save(orders: List[Order])(implicit session: Session): Unit = tableQuery ++= orders

    override def update(order: Order)(implicit session: Session): Unit = ???

    override def findById(id: UUID)(implicit session: Session): Option[Order] = tableQuery.filter(_.id === id).firstOption

    override def delete(id: UUID)(implicit session: Session): Unit = ???

    override def list(page: Int, length: Int, query: Option[String])(implicit session: Session): Page[Order] = ???

    override def listAll()(implicit session: Session): List[Order] = tableQuery.list

    override def latestOrders(quantity: Int)(implicit session: Session): List[Order] =
      tableQuery.sortBy(_.finishTime.desc).take(quantity).list

    override def listRankedBooze()(implicit session: Session): List[RankedBooze] = {

      implicit val getRankedBoozeResult = GetResult(r => RankedBooze(r.<<, r.<<, r.<<, r.<<))

      StaticQuery.queryNA[RankedBooze](
        """SELECT
            COUNT(1),
            boozecode,
            boozename,
            'Classic'
           FROM
            "order"
           GROUP BY boozecode,
                    boozename
           ORDER BY COUNT(1) DESC""").list
    }

    override def countByYear(date: DateTime)(implicit session: Session): Int = {

      def byYear(column: Column[DateTime]) = {
        column >= date.withMonthOfYear(JANUARY).dayOfMonth().withMinimumValue() &&
          column <= date.withMonthOfYear(DECEMBER).dayOfMonth().withMaximumValue()
      }

      tableQuery.filter(o => byYear(o.finishTime)).length.run
    }

    override def countByMonth(date: DateTime)(implicit session: Session): Int = {

      def byMonth(column: Column[DateTime]) = {
        column >= date.dayOfMonth().withMinimumValue() &&
          column <= date.dayOfMonth().withMaximumValue()
      }
      tableQuery.filter(o => byMonth(o.finishTime)).length.run
    }

    override def countByDay(date: DateTime)(implicit session: Session): Int =
      tableQuery.filter(_.finishTime === date).length.run
  }

}
