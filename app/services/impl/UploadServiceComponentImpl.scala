package services.impl

import java.io.File

import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.PutObjectRequest
import services.UploadServiceComponent
import utils.AppConfig._

/**
 * Created by gbmetzner on 16/08/14.
 */
trait UploadServiceComponentImpl extends UploadServiceComponent {

  override val uploadService: UploadService = new UploadServiceImpl

  class UploadServiceImpl extends UploadService {

    private val s3 = new AmazonS3Client(new BasicAWSCredentials(s3AccessKey, s3AccessSecret))

    override def upload(bucketName: String, key: String, file: File): Unit = s3 putObject (new PutObjectRequest(bucketName, key, file))
  }

}
