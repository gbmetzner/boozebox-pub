$(document).ready(function () {
    $('#ingredients').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/admin/ingredient/pagination',
        columns: [
            { data: 'ingredient' },
            { data: 'density' },
            { data: 'createdOn' },
            { data: 'updatedOn' },
            { data: 'imagePath' },
            { data: 'id' }
        ],
        columnDefs: [
            {
                targets: 4,
                data: null,
                render: function (data, type, full, meta) {
                    return '<img src="' + data + '" height="32" width="32" />';
                }
            },
            {
                targets: -1,
                data: "Edit",
                render: function (data, type, full, meta) {
                    return '<a href="' + data + '/edit">Edit</a> - <button value="' + data + '" class="delete">Delete</button>';
                }
            }
        ]
    });

    $("#ingredients tbody").on('click', 'button', function () {
        var id = $(this).val();
        $("#dialog-confirm").children().empty();
        $("#dialog-confirm").children().append('<span class="ui-icon ui-icon-alert" style="float : left ; margin : 0 7px 20px 0 ;"></span>');
        $("#dialog-confirm").children().append('This item will be permanently deleted and cannot be recovered. Are you sure?');

        $("#dialog-confirm").dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                "Delete ingredient": function () {
                    $.ajax({
                        type: 'POST',
                        data: null,
                        url: '/admin/ingredient/delete/' + id,
                        dataType: 'json',
                        headers: { 'Content-Type': 'application/text' },
                        success: function (data) {
                            $("#dialog-confirm").children().empty();
                            $("#dialog-confirm").children().append(data.message);
                            $("#dialog-confirm").dialog({
                                modal: true,
                                closeOnEscape: false,
                                buttons: {
                                    Ok: function () {
                                        window.location.href = '/admin/ingredient/list';
                                    }
                                }
                            });
                        },
                        error: function (xhr, status, error) {
                            var err = eval("(" + xhr.responseText + ")");
                            $("#dialog-confirm").children().empty();
                            $("#dialog-confirm").children().append(err.message);
                            $("#dialog-confirm").dialog({
                                modal: true,
                                closeOnEscape: false,
                                buttons: {
                                    Ok: function () {
                                        $("#dialog-confirm").dialog("close");
                                    }
                                }
                            });
                        }
                    });
                    $(this).dialog("close");
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    });
});