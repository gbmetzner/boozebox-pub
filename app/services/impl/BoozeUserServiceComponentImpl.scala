package services.impl

import java.util.UUID

import models.admin.BoozeUser
import repositories.BoozeUserRepositoryComponent
import services.BoozeUserServiceComponent
import utils.paginate.Page

/**
 * Created by gbmetzner on 2/26/15.
 */
trait BoozeUserServiceComponentImpl extends BoozeUserServiceComponent {
  requires: BoozeUserRepositoryComponent =>
  override def boozeUserService: BoozeUserService = new BoozeServiceImpl

  class BoozeServiceImpl extends BoozeUserService {

    override def authenticate(username: String, password: String): Option[BoozeUser] = db.withSession {
      implicit session =>
        boozeUserRepository authenticate(username, password)
    }

    override def persist(t: BoozeUser): Unit = ???

    override def findById(id: UUID): Option[BoozeUser] = ???

    override def delete(id: UUID): Unit = ???

    override def list(page: Int, length: Int, query: Option[String]): Page[BoozeUser] = ???

    override def listAll: List[BoozeUser] = ???
  }

}
