package repositories

import models.device.Device

import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gbmetzner on 22/11/14.
 */
trait DeviceRepositoryComponent {

  def deviceRepository: DeviceRepository

  trait DeviceRepository extends Repository[Device] {
    def findByCode(code: String)(implicit session: Session): Option[Device]
  }

}
