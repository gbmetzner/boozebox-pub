package wsrestful

import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import services.DeviceServiceComponent
import services.registry.DeviceRegistry

/**
 * Created by gbmetzner on 27/11/14.
 */
object DeviceWS extends DeviceWS with DeviceRegistry

trait DeviceWS extends Controller {
  self: DeviceServiceComponent =>

  def online(code: String) = Action {

    deviceService findByCode code match {
      case Some(d) => Ok(Json.obj("result" -> deviceService.online(d.code)))
      case None => NotFound
    }
  }
}
