package services

import models.instruction.Instruction

/**
 * Created by gbmetzner on 19/09/14.
 */
trait InstructionServiceComponent {

  def instructionService: InstructionService

  trait InstructionService extends ServiceComponent[Instruction]

}
