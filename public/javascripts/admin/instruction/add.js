$(document).ready(function () {

    $('#instructionForm').bootstrapValidator().on('success.form.bv', function (e) {

        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        var data = JSON.stringify({
            read: $('#read').val(),
            speak: $('#speak').val()
        });

        $.ajax({
            type: 'POST',
            data: data,
            url: $form.attr('action'),
            dataType: 'json',
            headers: { 'Content-Type': 'application/json' },
            success: function (data) {
                $("#dialog").children().empty();
                $("#dialog").children().append(data.message);
                $("#dialog").dialog({
                    modal: true,
                    closeOnEscape: false,
                    dialogClass: 'noclose',
                    buttons: {
                        Ok: function () {
                            window.location.href = '/admin/instruction/list';
                        }
                    }
                });
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                $("#dialog").children().empty();
                $("#dialog").children().append(err.message);
                $("#dialog").dialog({
                    modal: true,
                    closeOnEscape: false,
                    dialogClass: 'noclose',
                    buttons: {
                        Ok: function () {
                            $("#dialog").dialog("close");
                        }
                    }
                });
            }
        });
    });
});