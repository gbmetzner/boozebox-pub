package services

import models.cup.Glass

/**
 * Created by gbmetzner on 10/08/14.
 */
trait GlassServiceComponent {

  def glassService: GlassService

  trait GlassService extends ServiceComponent[Glass]
}
