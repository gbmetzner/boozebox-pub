package services.registry

import repositories.impl.BoozeRepositoryComponentImpl
import services.impl.{BoozeServiceComponentImpl, UploadServiceComponentImpl}

/**
 * Created by gbmetzner on 24/08/14.
 */
trait BoozeRegistry extends BoozeServiceComponentImpl with BoozeRepositoryComponentImpl
with GlassRegistry with UploadServiceComponentImpl
