package utils

import play.api.Play.current

/**
 * Created by gbmetzner on 12/06/14.
 */
object AppConfig {

  private val config = play.api.Play.configuration

  // Facebook
  lazy val facebookClientId = config.getString("facebook.client.id").getOrElse(throw new IllegalArgumentException("key: facebook.client.id not found."))
  lazy val facebookClientSecret = config.getString("facebook.client.secret").getOrElse(throw new IllegalArgumentException("key: facebook.client.secret not found."))
  lazy val facebookRedirectUrl = config.getString("facebook.redirect.url").getOrElse(throw new IllegalArgumentException("key: facebook.redirect.url not found."))
  lazy val facebookAccessTokenUrl = config.getString("facebook.access.token.url").getOrElse(throw new IllegalArgumentException("key: facebook.access.token.url not found."))
  lazy val facebookLoginUrl = config.getString("facebook.login.url").getOrElse(throw new IllegalArgumentException("key: facebook.login.url not found."))
  lazy val facebookPostUrl = config.getString("facebook.post.url").getOrElse(throw new IllegalArgumentException("key: facebook.post.url not found."))
  lazy val facebookGetMetUrl = config.getString("facebook.get.me.url").getOrElse(throw new IllegalArgumentException("key: facebook.get.me.url not found."))
  lazy val facebookPostMessage = config.getString("facebook.post.msg").getOrElse(throw new IllegalArgumentException("key: facebook.post.msg not found."))
  lazy val facebookPostName = config.getString("facebook.post.name").getOrElse(throw new IllegalArgumentException("key: facebook.post.name not found."))
  lazy val facebookPostLink = config.getString("facebook.post.link").getOrElse(throw new IllegalArgumentException("key: facebook.post.link not found."))
  lazy val facebookPostDescription = config.getString("facebook.post.description").getOrElse(throw new IllegalArgumentException("key: facebook.post.description not found."))
  lazy val facebookPostCaption = config.getString("facebook.post.caption").getOrElse(throw new IllegalArgumentException("key: facebook.post.caption not found."))

  // Android
  lazy val androidDeviceId = config.getString("android.device.id").getOrElse(throw new IllegalArgumentException("key: android.device.id not found."))
  lazy val androidApiKey = config.getString("android.api.key").getOrElse(throw new IllegalArgumentException("key: android.api.key not found."))

  // S3
  lazy val s3AccessKey = config.getString("s3.access.key").getOrElse(throw new IllegalArgumentException("key: s3.access.key not found."))
  lazy val s3AccessSecret = config.getString("s3.access.secret").getOrElse(throw new IllegalArgumentException("key: s3.access.secret not found."))
  lazy val s3LinkDrink = config.getString("s3.link.drink").getOrElse(throw new IllegalArgumentException("key: s3.link.drink not found."))
  lazy val s3Bucket = config.getString("s3.bucket.name").getOrElse(throw new IllegalArgumentException("key: s3.bucket.name not found."))
  lazy val s3CupUploadKey = config.getString("s3.cup.upload.key").getOrElse(throw new IllegalArgumentException("key: s3.cup.upload.key not found."))
  lazy val s3IngredientUploadKey = config.getString("s3.ingredient.upload.key").getOrElse(throw new IllegalArgumentException("key: s3.ingredient.upload.key not found."))
  lazy val s3CocktailUploadKey = config.getString("s3.cocktail.upload.key").getOrElse(throw new IllegalArgumentException("key: s3.cocktail.upload.key not found."))
  lazy val s3Path = config.getString("s3.path").getOrElse(throw new IllegalArgumentException("key: s3.path not found."))

  // DB
  lazy val dbUrl = config.getString("db.mysql.url").getOrElse(throw new IllegalArgumentException("key: db.mysql.url not found."))
  lazy val dbDriver = config.getString("db.mysql.driver").getOrElse(throw new IllegalArgumentException("key: db.mysql.driver not found."))
  lazy val dbUser = config.getString("db.mysql.user").getOrElse(throw new IllegalArgumentException("key: db.mysql.user not found."))
  lazy val dbPass = config.getString("db.mysql.password").getOrElse(throw new IllegalArgumentException("key: db.mysql.password not found."))

}
