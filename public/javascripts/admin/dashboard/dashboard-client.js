
function loadLatest() {

    $.getJSON("/admin/dashboard/stats/latest/4", function (data) {

        $("#latest tbody").empty();

        $.each(data, function (index, value) {

            var content = "<tr> " +
                "<td style='vertical-align:middle' >" + value.orderId + "</td> " +
                "<td class='hidden-sm'> " +
                "<a href='javascript:;''> " +
                " <img src='" + value.img + "' alt='' style='width:90px; height:69px' /> " +
                "</a> " +
                "</td> " +
                "<td style='vertical-align:middle'> " +
                "<divx><font size='4'>" + value.boozeName + "</font></divx> " +
                "</td> " +
                "<td style='vertical-align:middle'> " +
                value.inputTime + "<br> " +
                value.startTime + "<br> " +
                value.finishTime +
                "</td> " +
                "<td style='vertical-align:middle'>" + value.owner + "</td> " +
                "</tr> ";

            $("#latest tbody").append(content);
        });

    });

    setTimeout('loadLatest()',5000);
}


function loadDrinkRanking() {

    $.getJSON("/admin/dashboard/stats/ranking", function (data) {

        $("#rdrink tbody").empty();

        $.each(data, function (index, value) {

            var content = "<tr> " +
                "<td style='vertical-align:middle' >" + value.cocktailId + "</td> " +
                "<td class='hidden-sm'> " +
                "<a href='javascript:;''> " +
                " <img src='" + value.img + "' alt='' style='width:90px; height:69px' /> " +
                "</a> " +
                "</td> " +
                "<td style='vertical-align:middle'> " +
                "<divx><font size='4'>" + value.cocktailName + "</font></divx> " +
                "</td> " +
                "<td style='vertical-align:middle'> " + value.cocktailType + "</td> " +
                "<td style='vertical-align:middle'>" + value.quantity + "</td> " +
                "</tr> ";

            $("#rdrink tbody").append(content);
        });

    });

    setTimeout('loadDrinkRanking()',5000);
}

function loadStatusByYear() {

    $.getJSON("/admin/dashboard/stats/year", function (data) {

        $("#stats-number-year").empty();
        $("#stats-number-year").append(data.countByYear);
        $("#progress-bar-year").empty();
        $("#progress-bar-year").css("width", data.percentIncrease + "%");
        $("#stats-desc-year").empty();
        $("#stats-desc-year").append(data.percentIncreaseMsg);

    });

    setTimeout('loadStatusByYear()',5000);
}

function loadStatusByMonth() {

    $.getJSON("/admin/dashboard/stats/month", function (data) {

        $("#stats-number-month").empty();
        $("#stats-number-month").append(data.countByMonth);
        $("#progress-bar-month").empty();
        $("#progress-bar-month").css("width", data.percentIncrease + "%");
        $("#stats-desc-month").empty();
        $("#stats-desc-month").append(data.percentIncreaseMsg);

    });

    setTimeout('loadStatusByMonth()',5000);
}

function loadStatusByWeek() {

    $.getJSON("/admin/dashboard/stats/week", function (data) {

        $("#stats-number-week").empty();
        $("#stats-number-week").append(data.countByWeek);
        $("#progress-bar-week").empty();
        $("#progress-bar-week").css("width", data.percentIncrease + "%");
        $("#stats-desc-week").empty();
        $("#stats-desc-week").append(data.percentIncreaseMsg);

    });

    setTimeout('loadStatusByWeek()',5000);
}

function loadStatusByDay() {

    $.getJSON("/admin/dashboard/stats/day", function (data) {

        $("#stats-number-day").empty();
        $("#stats-number-day").append(data.countByDay);
        $("#progress-bar-day").empty();
        $("#progress-bar-day").css("width", data.percentIncrease + "%");
        $("#stats-desc-day").empty();
        $("#stats-desc-day").append(data.percentIncreaseMsg);

    });

    setTimeout('loadStatusByDay()',5000);
}


loadLatest();

loadDrinkRanking();

loadStatusByYear();

loadStatusByMonth();

loadStatusByWeek();

loadStatusByDay();

