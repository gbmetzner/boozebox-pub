package wsrestful

import controllers.admin.BaseController
import play.api.libs.json.Json
import play.api.mvc.Action
import services.BoozeServiceComponent
import services.registry.BoozeRegistry

/**
 * Created by gbmetzner on 09/11/14.
 */
object BoozeWS extends BoozeWS with BoozeRegistry

trait BoozeWS extends BaseController {
  self: BoozeServiceComponent =>

  /**
   * return all cocktails.
   */
  def all = Action {
    import utils.parsers.boozeFormat
    Ok(Json toJson (boozeService listAll))
  }

}
