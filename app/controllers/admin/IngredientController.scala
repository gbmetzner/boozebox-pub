package controllers.admin

import java.io.File
import java.util.UUID

import models.ingredient.Ingredient
import play.api.libs.json.{JsError, Json}
import play.api.mvc.{Action, BodyParsers}
import services.registry.IngredientRegistry
import services.{IngredientServiceComponent, UploadServiceComponent}
import utils.AppConfig._
import views.html

/**
 * Created by gbmetzner on 17/08/14.
 */
object IngredientController extends IngredientController with IngredientRegistry

trait IngredientController extends BaseController {
  this: IngredientServiceComponent with UploadServiceComponent =>

  import utils.parsers.ingredientFormat

  def form = Action {
    Ok(html.admin.ingredient.add())
  }

  def save = Action(BodyParsers.parse.json) {
    implicit request =>

      request.body.validate[Ingredient].fold(
        errors => BadRequest(Json.obj("status" -> "400", "message" -> JsError.toFlatJson(errors))),
        ingredient => {
          ingredientService persist ingredient
          Ok(Json.obj("status" -> "OK", "message" -> s"Ingredient: ${ingredient.ingredient} saved."))
        }
      )
  }

  def update(id: UUID) = Action(BodyParsers.parse.json) {
    implicit request =>

      request.body.validate[Ingredient].fold(
        errors => BadRequest(Json.obj("status" -> "400", "message" -> JsError.toFlatJson(errors))),
        ingredient => {
          ingredientService persist ingredient
          Ok(Json.obj("status" -> "OK", "message" -> s"Ingredient: ${ingredient.ingredient} updated."))
        }
      )
  }

  def upload = Action(parse.multipartFormData) {
    implicit request =>

      request.body.file("image").map {
        image =>

          val filename = image.filename
          val file = new File(s"/tmp/$filename")

          image.ref.moveTo(file)

          uploadService upload(s3Bucket, s"$s3IngredientUploadKey${filename}", file)

          file.delete

          Ok("Ok")
      }.getOrElse {
        NotFound
      }
  }

  /**
   * Find the Cup
   *
   * @param id Current id of the Cup to be updated
   */
  def edit(id: UUID) = Action {
    implicit request =>
      ingredientService findById id match {
        case Some(ingredient) => Ok(html.admin.ingredient.edit(ingredient))
        case None => NotFound
      }
  }

  /**
   * Display the page list of Cups.
   */
  def listPage = Action {
    implicit request =>
      Ok(html.admin.ingredient.list())
  }

  /**
   * Display the list of Cups by page.
   */
  def pagination = Action {
    implicit request =>

      val (draw, start, length, query) = parseQueryStringPagination(request.queryString)

      val ingredients = ingredientService list(start, length, query)

      val ingredientsJson = ingredients.items.map(i => Json.toJson(i))

      Ok(Json.obj("draw" -> draw, "recordsTotal" -> ingredients.total, "recordsFiltered" -> ingredients.total, "data" -> ingredientsJson))
  }

  /**
   * Delete the Cup
   *
   * @param id Current id of the Cup to be deleted.
   */
  def delete(id: UUID) = Action {
    implicit request =>
      ingredientService delete id
      Ok(Json.obj("message" -> "Ingredient was deleted."))
  }


}
