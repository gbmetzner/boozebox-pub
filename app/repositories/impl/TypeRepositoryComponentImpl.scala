package repositories.impl

import java.util.UUID

import models.drinktype.{Type, TypeTable}
import org.joda.time.DateTime
import repositories.TypeRepositoryComponent
import utils.uuid._
import utils.paginate.Page

import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gbmetzner on 09/08/14.
 */
trait TypeRepositoryComponentImpl extends TypeRepositoryComponent {

  override def typeRepository: TypeRepository = new TypeRepositoryImpl

  class TypeRepositoryImpl extends TypeRepository {

    private val tableQuery = TableQuery[TypeTable]

    override def findById(id: UUID)(implicit s: Session): Option[Type] = {
      tableQuery filter (_.id === id) firstOption
    }

    override def listAll()(implicit s: Session): List[Type] = tableQuery.list

    override def update(tpe: Type)(implicit s: Session): Unit = {
      tableQuery.filter(_.id === tpe.id)
        .map(dt => (dt.description, dt.updatedAt))
        .update((tpe.description, Some(DateTime.now)))
    }

    override def save(tpe: Type)(implicit s: Session): Unit = {
      val now = Some(DateTime.now)
      tableQuery += tpe.copy(id = Some(uuid), createdAt = now, updatedAt = now)
    }

    override def list(page: Int, length: Int, query: Option[String] = None)(implicit s: Session): Page[Type] = {

      val filtered = query match {
        case Some(query) => tableQuery.filter(_.description.toLowerCase like s"%${query.toLowerCase}%")
        case None => tableQuery
      }

      val total = filtered.length.run

      val types = filtered.drop(page).take(length).list

      Page(types, page, page, total)
    }

    override def delete(id: UUID)(implicit s: Session): Unit = {
      tableQuery filter (_.id === id) delete
    }
  }

}