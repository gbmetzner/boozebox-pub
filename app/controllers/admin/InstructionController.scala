package controllers.admin

import java.util.UUID

import models.instruction.Instruction
import play.api.libs.json.{JsError, Json}
import play.api.mvc.{Action, BodyParsers}
import services.InstructionServiceComponent
import services.registry.InstructionRegistry
import views.html

/**
 * Created by gbmetzner on 19/09/14.
 */
object InstructionController extends InstructionController with InstructionRegistry

trait InstructionController extends BaseController {
  this: InstructionServiceComponent =>

  import utils.parsers.instructionFormat

  def form = Action {
    Ok(html.admin.instruction.add())
  }

  def save = Action(BodyParsers.parse.json) {
    implicit request =>

      request.body.validate[Instruction].fold(
        errors => BadRequest(Json.obj("status" -> "400", "message" -> JsError.toFlatJson(errors))),
        instruction => {
          instructionService persist instruction
          Ok(Json.obj("status" -> "OK", "message" -> s"Instruction: ${instruction.read} - ${instruction.speak} saved."))
        }
      )
  }

  def update(id: UUID) = Action(BodyParsers.parse.json) {
    implicit request =>

      request.body.validate[Instruction].fold(
        errors => {
          BadRequest(Json.obj("status" -> "400", "message" -> JsError.toFlatJson(errors)))
        },
        instruction => {
          instructionService persist instruction
          Ok(Json.obj("status" -> "OK", "message" -> s"Instruction: ${instruction.read} - ${instruction.speak} saved."))
        }
      )
  }

  /**
   * Find the Cup
   *
   * @param id Current id of the Cup to be updated
   */
  def edit(id: UUID) = Action {
    implicit request =>
      instructionService findById id match {
        case Some(instruction) => Ok(html.admin.instruction.edit(instruction))
        case None => NotFound
      }
  }

  /**
   * Display the page list of Cups.
   */
  def listPage = Action {
    implicit request =>
      logger info "listing instructions..."
      Ok(html.admin.instruction.list())
  }

  /**
   * Display the list of Cups by page.
   */
  def pagination = Action {
    implicit request =>

      val (draw, start, length, query) = parseQueryStringPagination(request.queryString)

      logger info query.toString

      val instruction = instructionService list(start, length, query)

      val instructionsJson = instruction.items.map(c => Json.toJson(c))

      Ok(Json.obj("draw" -> draw, "recordsTotal" -> instruction.total, "recordsFiltered" -> instruction.total, "data" -> instructionsJson))
  }

  /**
   * Delete the Cup
   *
   * @param id Current id of the Cup to be deleted.
   */
  def delete(id: UUID) = Action {
    implicit request =>
      instructionService delete id
      Ok(Json.obj("message" -> "instruction was deleted."))
  }
}
