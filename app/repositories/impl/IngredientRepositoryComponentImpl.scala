package repositories.impl

import java.util.UUID

import models.ingredient.{Ingredient, IngredientTable}
import org.joda.time.DateTime
import repositories.IngredientRepositoryComponent
import utils.uuid._
import utils.paginate.Page

import scala.slick.driver.PostgresDriver.simple._
import scala.slick.lifted.TableQuery

/**
 * Created by gbmetzner on 17/08/14.
 */
trait IngredientRepositoryComponentImpl extends IngredientRepositoryComponent {

  override def ingredientRepository: IngredientRepository = new IngredientRepositoryImpl

  class IngredientRepositoryImpl extends IngredientRepository {

    private val tableQuery = TableQuery[IngredientTable]

    override def save(ingredient: Ingredient)(implicit s: Session): Unit = {
      val now = Some(DateTime.now)
      tableQuery += ingredient.copy(id = Some(uuid), createdAt = now, updatedAt = now)
    }

    override def update(ingredient: Ingredient)(implicit s: Session): Unit = {
      tableQuery.filter(_.id === ingredient.id)
        .map(i => (i.ingredient, i.density, i.updatedAt))
        .update((ingredient.ingredient, ingredient.density, Some(DateTime.now)))

    }

    override def findById(id: UUID)(implicit s: Session): Option[Ingredient] = {
      tableQuery.filter(_.id === id).firstOption
    }

    override def delete(id: UUID)(implicit s: Session): Unit = {
      tableQuery.filter(_.id === id).delete
    }

    override def list(page: Int, length: Int, query: Option[String] = None)(implicit s: Session): Page[Ingredient] = {

      val ingredients = tableQuery.drop(page).take(length).list

      val total = tableQuery.length.run

      Page(ingredients, page, page, total)
    }

    override def listAll()(implicit s: Session): List[Ingredient] = tableQuery.list

  }

}
