package utils

import java.util.UUID

import models.booze.Booze
import models.cocktype.BoozeType
import models.cup.Glass
import models.device.Device
import models.drinktype.Type
import models.dto.{WSOrder, WebOrder, RankedBooze}
import models.ingredient.Ingredient
import models.instruction.Instruction
import models.Order
import play.api.libs.json.Reads._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import utils.converters._
/**
 * Created by gbmetzner on 31/08/14.
 */
package object parsers {

  private val glassWrites = new Writes[Glass] {
    def writes(glass: Glass) = Json.obj(
      "id" -> glass.id,
      "name" -> glass.name,
      "size" -> glass.size,
      "createdAt" -> glass.createdAt,
      "updatedAt" -> glass.updatedAt,
      "imageFull" -> glass.imageFull
    )
  }

  implicit val glassFormat: Format[Glass] = Format(Json.reads[Glass], glassWrites)

  implicit val typeFormat: Format[Type] = Format(Json.reads[Type], Json.writes[Type])

  private val ingredientWrites = new Writes[Ingredient] {
    def writes(ingredient: Ingredient) = Json.obj(
      "id" -> ingredient.id,
      "ingredient" -> ingredient.ingredient,
      "density" -> ingredient.density,
      "createdAt" -> ingredient.createdAt,
      "updatedAt" -> ingredient.updatedAt,
      "imageFull" -> ingredient.imageFull
    )
  }

  implicit val ingredientFormat: Format[Ingredient] = Format(Json.reads[Ingredient], ingredientWrites)

  private val boozeWrites = new Writes[Booze] {
    def writes(booze: Booze) = Json.obj(
      "id" -> booze.id,
      "name" -> booze.name,
      "deviceCode" -> booze.deviceCode,
      "author" -> booze.author,
      "imageFull" -> booze.imageFull,
      "createdAt" -> booze.createdAt,
      "updatedAt" -> booze.updatedAt
    )
  }

  implicit val boozeFormat: Format[Booze] = Format(Json.reads[Booze], boozeWrites)

  implicit val boozeTypeFormat: Format[BoozeType] = Format(Json.reads[BoozeType], Json.writes[BoozeType])

  implicit val instructionFormat: Format[Instruction] = Format(Json.reads[Instruction], Json.writes[Instruction])

  implicit val deviceFormat: Format[Device] = Format(Json.reads[Device], Json.writes[Device])

  implicit val webOrderReads: Reads[WebOrder] = (
    (__ \ "name").read[String](minLength[String](3)) ~
      (__ \ "voucher").readNullable[String] ~
      (__ \ "cocktailId").read[UUID] ~
      (__ \ "postOnFB").read[Boolean]
    )(WebOrder.apply _)

  implicit val boozeSelectWrites = new Writes[Booze] {
    def writes(booze: Booze) =
      Json.obj("text" -> booze.name,
        "value" -> booze.id,
        "selected" -> false,
        "imageSrc" -> booze.imageFull
      )
  }

  implicit val rankedBoozeWrites = new Writes[RankedBooze] {
    def writes(rankedBooze: RankedBooze) = Json.obj(
      "cocktailId" -> rankedBooze.cocktailId,
      "cocktailName" -> rankedBooze.cocktailName,
      "cocktailType" -> rankedBooze.cocktailType,
      "quantity" -> rankedBooze.quantity,
      "img" -> s"https://s3-sa-east-1.amazonaws.com/bbcampaign/cocktail/thumb/${rankedBooze.cocktailId}.png"
    )
  }

  implicit val latestOrdersWrites = new Writes[Order] {
    def writes(order: Order) = Json.obj(
      "orderId" -> order.orderId,
      "owner" -> order.owner,
      "boozeCode" -> order.boozeCode,
      "boozeName" -> order.boozeName,
      "voucher" -> order.voucher,
      "inputTime" -> order.inputTime.get.toString(fmtFullDateTime),
      "startTime" -> order.startTime.toString(fmtFullDateTime),
      "finishTime" -> order.finishTime.toString(fmtFullDateTime),
      "machine" -> order.machine,
      "img" -> s"https://s3-sa-east-1.amazonaws.com/bbcampaign/cocktail/thumb/${order.boozeCode}.png"
    )
  }

  implicit val mobileOrderReads: Reads[WSOrder] = (
    (__ \ "id_order").read[String] ~
      (__ \ "owner").read[String] ~
      (__ \ "id_cocktail").read[String] ~
      (__ \ "cocktailName").read[String] ~
      (__ \ "voucher").read[String] ~
      (__ \ "input_time").read[String] ~
      (__ \ "start_time").read[String] ~
      (__ \ "finish_time").read[String] ~
      (__ \ "machine").read[String]
    )(WSOrder.apply _)
}

