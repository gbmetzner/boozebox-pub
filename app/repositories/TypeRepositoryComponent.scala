package repositories

import models.drinktype.Type

/**
 * Created by gbmetzner on 09/08/14.
 */
trait TypeRepositoryComponent {

  def typeRepository: TypeRepository

  trait TypeRepository extends Repository[Type]

}
