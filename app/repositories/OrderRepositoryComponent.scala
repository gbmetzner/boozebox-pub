package repositories

import models.Order
import models.dto.RankedBooze
import org.joda.time.DateTime

import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gbmetzner on 08/02/15.
 */
trait OrderRepositoryComponent {

  def orderRepository: OrderRepository

  trait OrderRepository extends Repository[Order] {
    def save(orders: List[Order])(implicit session: Session): Unit

    def latestOrders(quantity: Int)(implicit session: Session): List[Order]

    def listRankedBooze()(implicit session: Session): List[RankedBooze]

    def countByYear(date: DateTime)(implicit session: Session): Int

    def countByMonth(date: DateTime)(implicit session: Session): Int

    def countByDay(date: DateTime)(implicit session: Session): Int
  }

}
