import play.PlayScala

name := """boozebox"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.6"

resolvers += Resolver.url("Edulify Repository", url("http://edulify.github.io/modules/releases/"))(Resolver.ivyStylePatterns)

val lastIntegration = "latest.integration"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "com.ganyo" % "gcm-server" % lastIntegration,
  "com.google.android" % "android" % lastIntegration,
  "org.postgresql" % "postgresql" % lastIntegration,
  "jp.t2v" %% "play2-auth"      % lastIntegration,
  "jp.t2v" %% "play2-auth-test" % lastIntegration % "test",
  "com.amazonaws" % "aws-java-sdk" % lastIntegration,
  "com.typesafe.slick" %% "slick" % "2.1.0",
  "org.joda" % "joda-convert" % lastIntegration,
  "com.github.tototoshi" %% "slick-joda-mapper" % lastIntegration,
  "com.typesafe.scala-logging" %% "scala-logging" % lastIntegration,
  "net.debasishg" %% "redisclient" % lastIntegration
)

fork in run := true