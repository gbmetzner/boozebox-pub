package services.impl

import java.util.UUID

import models.ingredient.Ingredient
import repositories.IngredientRepositoryComponent
import services.{DBService, IngredientServiceComponent}
import utils.paginate.Page

/**
 * Created by gbmetzner on 17/08/14.
 */
trait IngredientServiceComponentImpl extends IngredientServiceComponent {
  self: IngredientRepositoryComponent =>

  override def ingredientService: IngredientService = new IngredientServiceImpl

  class IngredientServiceImpl extends IngredientService with DBService {

    override def persist(ingredient: Ingredient): Unit = db.withTransaction {
      implicit session =>
        ingredient.id match {
          case Some(_) => ingredientRepository update ingredient
          case None => ingredientRepository save ingredient
        }
    }

    override def list(page: Int, length: Int, query: Option[String]): Page[Ingredient] = db.withSession {
      implicit session =>
        ingredientRepository list(page, length)
    }

    override def listAll: List[Ingredient] = db.withSession {
      implicit session =>
        ingredientRepository listAll()
    }

    override def findById(id: UUID): Option[Ingredient] = db.withSession {
      implicit session =>
        ingredientRepository findById id
    }

    override def delete(id: UUID): Unit = db.withTransaction {
      implicit session =>
        ingredientRepository delete id
    }
  }

}
