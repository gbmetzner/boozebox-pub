package services.registry

import repositories.impl.IngredientRepositoryComponentImpl
import services.impl.{UploadServiceComponentImpl, IngredientServiceComponentImpl}

/**
 * Created by gbmetzner on 17/08/14.
 */
trait IngredientRegistry extends IngredientServiceComponentImpl with IngredientRepositoryComponentImpl with UploadServiceComponentImpl
