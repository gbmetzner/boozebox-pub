package services.impl

import com.google.android.gcm.server.{Message, Sender}
import services.AndroidMessengerComponent
import utils.AppConfig

/**
 * Created by gbmetzner on 29/11/14.
 */
trait AndroidMessengerComponentImpl extends AndroidMessengerComponent {

  override def androidMessenger: AndroidMessenger = new AndroidMessengerImpl

  class AndroidMessengerImpl extends AndroidMessenger {
    override def sendMessage(codeDevice: String, data: (String, String)*): Boolean = {

      val sender = new Sender(AppConfig.androidApiKey)

      var message = new Message.Builder()
        .collapseKey("1")
        .timeToLive(3)
        .delayWhileIdle(true)

      data.foreach { case (key, msg) => message = message.addData(key, msg)}

      sender.send(message.build(), codeDevice, 1).getErrorCodeName

      true
    }
  }

}
