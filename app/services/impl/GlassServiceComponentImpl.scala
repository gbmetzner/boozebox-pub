package services.impl

import java.util.UUID

import models.cup.Glass
import repositories.GlassRepositoryComponent
import services.{GlassServiceComponent, DBService}
import utils.paginate.Page

/**
 * Created by gbmetzner on 10/08/14.
 */
trait GlassServiceComponentImpl extends GlassServiceComponent {
  requires: GlassRepositoryComponent =>

  override def glassService = new GlassServiceImpl

  class GlassServiceImpl extends GlassService {

    override def persist(glass: Glass): Unit = db.withTransaction {
      implicit session =>
        glass.id match {
          case Some(x) => glassRepository update glass
          case None => glassRepository save glass
        }
    }

    override def list(page: Int, length: Int = 10, query: Option[String] = None): Page[Glass] = db.withSession {
      implicit session =>
        glassRepository list(page, length, query)
    }

    override def findById(id: UUID): Option[Glass] = db.withSession {
      implicit session =>
        glassRepository findById id
    }

    override def delete(id: UUID): Unit = db.withTransaction {
      implicit session =>
        glassRepository delete id
    }

    override def listAll: List[Glass] = db.withSession {
      implicit session =>
        glassRepository listAll()
    }
  }

}
