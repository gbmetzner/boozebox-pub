package services

import java.io.File

/**
 * Created by gbmetzner on 16/08/14.
 */
trait UploadServiceComponent {
  val uploadService: UploadService

  trait UploadService {
    def upload(bucketName: String, key: String, file: File): Unit
  }

}
