$(document).ready(function () {
    $('#devices').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/admin/device/pagination',
        columns: [
            { data: 'code' },
            { data: 'description' },
            { data: 'createdAt' },
            { data: 'updatedAt' },
            { data: 'id' }
        ],
        columnDefs: [
            {
                targets: -1,
                data: "Edit",
                render: function (data, type, full, meta) {
                    return '<a href="' + data + '/edit">Edit</a> - <button value="' + data + '" class="delete">Delete</button>';
                }
            }
        ]
    });

    $("#devices tbody").on('click', 'button', function () {
        var id = $(this).val();
        $("#dialog-confirm").children().empty();
        $("#dialog-confirm").children().append('<span class="ui-icon ui-icon-alert" style="float : left ; margin : 0 7px 20px 0 ;"></span>');
        $("#dialog-confirm").children().append('This item will be permanently deleted and cannot be recovered. Are you sure?');

        $("#dialog-confirm").dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                "Delete Device": function () {
                    $.ajax({
                        type: 'DELETE',
                        data: null,
                        url: '/admin/device/' + id + '/delete',
                        dataType: 'json',
                        headers: { 'Content-Type': 'application/text' },
                        success: function (data) {
                            $("#dialog-confirm").children().empty();
                            $("#dialog-confirm").children().append(data.message);
                            $("#dialog-confirm").dialog({
                                modal: true,
                                closeOnEscape: false,
                                buttons: {
                                    Ok: function () {
                                        window.location.href = '/admin/device/list';
                                    }
                                }
                            });
                        },
                        error: function (xhr, status, error) {
                            var err = eval("(" + xhr.responseText + ")");
                            $("#dialog-confirm").children().empty();
                            $("#dialog-confirm").children().append(err.message);
                            $("#dialog-confirm").dialog({
                                modal: true,
                                closeOnEscape: false,
                                buttons: {
                                    Ok: function () {
                                        $("#dialog-confirm").dialog("close");
                                    }
                                }
                            });
                        }
                    });
                    $(this).dialog("close");
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    });
});