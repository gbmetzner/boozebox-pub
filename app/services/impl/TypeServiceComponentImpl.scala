package services.impl

import java.util.UUID

import models.drinktype.Type
import repositories.TypeRepositoryComponent
import services.TypeServiceComponent
import utils.paginate.Page

/**
 * Created by gbmetzner on 09/08/14.
 */
trait TypeServiceComponentImpl extends TypeServiceComponent {
  requires: TypeRepositoryComponent =>

  override def typeService = new TypeServiceImpl

  class TypeServiceImpl extends TypeService {
    override def persist(tpe: Type): Unit = db.withTransaction {
      implicit session =>
        tpe.id match {
          case Some(x) => typeRepository update tpe
          case None => typeRepository save tpe
        }
    }

    override def list(page: Int, length: Int, query: Option[String]): Page[Type] = db.withSession {
      implicit session =>
        typeRepository list(page, length)
    }

    override def listAll: List[Type] = db.withSession {
      implicit session =>
        typeRepository listAll()
    }

    override def findById(id: UUID): Option[Type] = db.withSession {
      implicit session =>
        typeRepository findById id
    }

    override def delete(id: UUID): Unit = db.withTransaction {
      implicit session =>
        typeRepository delete id
    }
  }

}
