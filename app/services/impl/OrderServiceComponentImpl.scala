package services.impl

import java.util.UUID

import models.Order
import models.dto.{WebOrder => OrderView, RankedBooze}
import org.joda.time.DateTime
import repositories.OrderRepositoryComponent
import services.{AndroidMessengerComponent, BoozeServiceComponent, OrderServiceComponent}
import utils.AppConfig
import utils.paginate.Page
import utils.plugins.Social

/**
 * Created by gbmetzner on 07/02/15.
 */
trait OrderServiceComponentImpl extends OrderServiceComponent {
  requires: BoozeServiceComponent with AndroidMessengerComponent with OrderRepositoryComponent =>

  override def orderService: OrderService = new OrderServiceImpl

  class OrderServiceImpl extends OrderService {

    private def postOnFB(accessToken: String) = Social.post(AppConfig.facebookPostMessage, accessToken)


    override def send(order: OrderView, accessToken: Option[String]): Unit = {

      val cocktail = boozeService findById order.cocktailId match {
        case Some(c) => c
        case None => throw new IllegalStateException(s"There is no cocktail for id = ${order.cocktailId}")
      }

      val idOrder = "id_order" -> s"${DateTime.now.getMillis}"
      val idCocktail = "id_cocktail" -> cocktail.deviceCode.toString
      val voucher = "voucher" -> "3332470" //order.voucher.getOrElse("")
      val description = "description" -> order.name

      val sent = androidMessenger sendMessage(AppConfig.androidDeviceId, idOrder, idCocktail, voucher, description)

      if (sent && order.postOnFB && accessToken.isDefined) postOnFB(accessToken.get)


    }

    override def logOrder(logOrders: Order): String = db.withTransaction {
      implicit session =>

        orderRepository save logOrders

        logOrders.machine

    }

    override def logOrders(logOrders: List[Order]): String = db.withTransaction {
      implicit session =>

        orderRepository save logOrders

        logOrders.headOption match {
          case Some(l) => l.machine
          case None => throw new IllegalStateException("Something went wrong... are there order to log?")
        }

    }

    override def latestOrders(quantity: Int): List[Order] = db.withSession {
      implicit session =>
        orderRepository latestOrders quantity
    }

    override def listRankedBooze(): List[RankedBooze] = db.withSession {
      implicit session =>
        orderRepository.listRankedBooze
    }

    private def stats(from: DateTime, to: DateTime)(count: DateTime => Int): (Int, Double) = {

      val limit = 100.0d

      val countByLastDate = count(from)
      val countByCurrentDate = count(to)

      def calculatePercent: Double = if (countByLastDate == 0d) countByLastDate
      else ((countByCurrentDate * limit) / countByLastDate) - limit


      (countByCurrentDate, calculatePercent)
    }

    override def statsByYear(): (Int, Double) = db.withSession {
      implicit session =>

        val from = DateTime.now.minusYears(1)
        val to = DateTime.now

        stats(from, to) {
          orderRepository.countByYear
        }

    }

    override def statsByMonth(): (Int, Double) = db.withSession {
      implicit session =>

        val from = DateTime.now.minusMonths(1)
        val to = DateTime.now

        stats(from, to) {
          orderRepository.countByMonth
        }
    }

    override def statsByWeek(): (Int, Double) = db.withSession {
      implicit session =>

        val from = DateTime.now.minusWeeks(1)
        val to = DateTime.now

        stats(from, to) {
          orderRepository.countByDay
        }
    }

    override def statsByDay(): (Int, Double) = db.withSession {
      implicit session =>

        val from = DateTime.now.minusDays(1)
        val to = DateTime.now

        stats(from, to) {
          orderRepository.countByDay
        }
    }

    override def persist(t: Order): Unit = ???

    override def findById(id: UUID): Option[Order] = ???

    override def delete(id: UUID): Unit = ???

    override def list(page: Int, length: Int, query: Option[String]): Page[Order] = ???

    override def listAll: List[Order] = ???
  }

}
