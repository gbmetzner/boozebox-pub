/*   
 Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2
 Version: 1.6.0
 Author: Sean Ngu
 Website: http://www.seantheme.com/color-admin-v1.6/admin/
 */

var handleGoogleMapSetting = function () {
    "use strict";
    var mapDefault;
    var marker;

    // Multiple Markers
    var markers = [
        ['Adega Santiago', -23.57116, -46.689063],
        ['Alto da Harmonia', -23.555688, -46.68781],
        ['Baretto', -23.56441, -46.669522],
        ['Skye Bar', -23.581681, -46.667024],
        ['Terraco Italia', -23.545627, -46.643137],
        ['The View Bar', -23.566648, -46.652951],
    ];

    // Info Window Content
    var infoWindowContent = [
        ['<div class="info_content">' +
                    '<h4>Adega Santiago</h4>' +
                    '<p>Rua Sampaio Vidal, 1072 - Jardim Paulistano, Sao Paulo - SP<br> Telefone:(11) 3081-5211</p>' +
                    '</div>'],
        ['<div class="info_content">' +
                    '<h4>Alto da Harmonia</h4>' +
                    '<p>Rua Harmonia, 271 - Pinheiros, São Paulo - SP<br> Telefone:(11) 3081-5211</p>' +
                    '</div>'],
        ['<div class="info_content">' +
                    '<h4>Baretto</h4>' +
                    '<p>Rua Vitório Fasano, 88 - Jardins, São Paulo - SP<br> Telefone:(11) 3896-4000</p>' +
                    '</div>'],
        ['<div class="info_content">' +
                    '<h4>Skye Bar</h4>' +
                    '<p>Av. Brigadeiro Luís Antônio, 4700 - Jardim Paulista, São Paulo - SP<br> Telefone:(11) 3055-4702</p>' +
                    '</div>'],
        ['<div class="info_content">' +
                    '<h4>Terraco Italia</h4>' +
                    '<p>Avenida Ipiranga, 344 - República, São Paulo - SP<br> Telefone:(11) 2189-2929</p>' +
                    '</div>'],
        ['<div class="info_content">' +
                    '<h4>The View Bar</h4>' +
                    '<p>Alameda Santos, 981 - Jardim Paulista, São Paulo - SP<br> Telefone:(11) 3266-3692</p>' +
                    '</div>']
    ];

    function initialize() {
        var mapOptions = {
//            zoom: 12,
//            center: new google.maps.LatLng(-23.5489196, -46.6330381),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };
        mapDefault = new google.maps.Map(document.getElementById('google-map-default'), mapOptions);
        
        mapDefault.setTilt(45);

        var infowindow = new google.maps.InfoWindow(), marker, i;

        var bounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            var pos = new google.maps.LatLng(markers[i][1], markers[i][2]);
            bounds.extend(pos);
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(markers[i][1], markers[i][2]),
                map: mapDefault
            });
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(infoWindowContent[i][0]);
                    infowindow.open(mapDefault, marker);
                }
            })(marker, i));
            mapDefault.fitBounds(bounds);
        }

    }
    google.maps.event.addDomListener(window, 'load', initialize);
    marker.setMap(map);
    $(window).resize(function () {
        google.maps.event.trigger(mapDefault, "resize");
    });

    var defaultMapStyles = [];
    var flatMapStyles = [{"stylers": [{"visibility": "off"}]}, {"featureType": "road", "stylers": [{"visibility": "on"}, {"color": "#ffffff"}]}, {"featureType": "road.arterial", "stylers": [{"visibility": "on"}, {"color": "#fee379"}]}, {"featureType": "road.highway", "stylers": [{"visibility": "on"}, {"color": "#fee379"}]}, {"featureType": "landscape", "stylers": [{"visibility": "on"}, {"color": "#f3f4f4"}]}, {"featureType": "water", "stylers": [{"visibility": "on"}, {"color": "#7fc8ed"}]}, {}, {"featureType": "road", "elementType": "labels", "stylers": [{"visibility": "off"}]}, {"featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{"visibility": "on"}, {"color": "#83cead"}]}, {"elementType": "labels", "stylers": [{"visibility": "off"}]}, {"featureType": "landscape.man_made", "elementType": "geometry", "stylers": [{"weight": 0.9}, {"visibility": "off"}]}];
    var turquoiseWaterStyles = [{"featureType": "landscape.natural", "elementType": "geometry.fill", "stylers": [{"visibility": "on"}, {"color": "#e0efef"}]}, {"featureType": "poi", "elementType": "geometry.fill", "stylers": [{"visibility": "on"}, {"hue": "#1900ff"}, {"color": "#c0e8e8"}]}, {"featureType": "landscape.man_made", "elementType": "geometry.fill"}, {"featureType": "road", "elementType": "geometry", "stylers": [{"lightness": 100}, {"visibility": "simplified"}]}, {"featureType": "road", "elementType": "labels", "stylers": [{"visibility": "off"}]}, {"featureType": "water", "stylers": [{"color": "#7dcdcd"}]}, {"featureType": "transit.line", "elementType": "geometry", "stylers": [{"visibility": "on"}, {"lightness": 700}]}];
    var icyBlueStyles = [{"stylers": [{"hue": "#2c3e50"}, {"saturation": 250}]}, {"featureType": "road", "elementType": "geometry", "stylers": [{"lightness": 50}, {"visibility": "simplified"}]}, {"featureType": "road", "elementType": "labels", "stylers": [{"visibility": "off"}]}];
    var oldDryMudStyles = [{"featureType": "landscape", "stylers": [{"hue": "#FFAD00"}, {"saturation": 50.2}, {"lightness": -34.8}, {"gamma": 1}]}, {"featureType": "road.highway", "stylers": [{"hue": "#FFAD00"}, {"saturation": -19.8}, {"lightness": -1.8}, {"gamma": 1}]}, {"featureType": "road.arterial", "stylers": [{"hue": "#FFAD00"}, {"saturation": 72.4}, {"lightness": -32.6}, {"gamma": 1}]}, {"featureType": "road.local", "stylers": [{"hue": "#FFAD00"}, {"saturation": 74.4}, {"lightness": -18}, {"gamma": 1}]}, {"featureType": "water", "stylers": [{"hue": "#00FFA6"}, {"saturation": -63.2}, {"lightness": 38}, {"gamma": 1}]}, {"featureType": "poi", "stylers": [{"hue": "#FFC300"}, {"saturation": 54.2}, {"lightness": -14.4}, {"gamma": 1}]}];
    var cobaltStyles = [{"featureType": "all", "elementType": "all", "stylers": [{"invert_lightness": true}, {"saturation": 10}, {"lightness": 10}, {"gamma": 0.8}, {"hue": "#293036"}]}, {"featureType": "water", "stylers": [{"visibility": "on"}, {"color": "#293036"}]}];
    var darkRedStyles = [{"featureType": "all", "elementType": "all", "stylers": [{"invert_lightness": true}, {"saturation": 10}, {"lightness": 10}, {"gamma": 0.8}, {"hue": "#000000"}]}, {"featureType": "water", "stylers": [{"visibility": "on"}, {"color": "#293036"}]}];

    $('[data-map-theme]').click(function () {
        var targetTheme = $(this).attr('data-map-theme');
        var targetLi = $(this).closest('li');
        var targetText = $(this).text();
        var inverseContentMode = false;
        $('#map-theme-selection li').not(targetLi).removeClass('active');
        $('#map-theme-text').text(targetText);
        $(targetLi).addClass('active');
        switch (targetTheme) {
            case 'flat':
                mapDefault.setOptions({styles: flatMapStyles});
                break;
            case 'turquoise-water':
                mapDefault.setOptions({styles: turquoiseWaterStyles});
                break;
            case 'icy-blue':
                mapDefault.setOptions({styles: icyBlueStyles});
                break;
            case 'cobalt':
                mapDefault.setOptions({styles: cobaltStyles});
                inverseContentMode = true;
                break;
            case 'old-dry-mud':
                mapDefault.setOptions({styles: oldDryMudStyles});
                break;
            case 'dark-red':
                mapDefault.setOptions({styles: darkRedStyles});
                inverseContentMode = true;
                break;
            default:
                mapDefault.setOptions({styles: defaultMapStyles});
                break;
        }

        if (inverseContentMode === true) {
            $('#content').addClass('content-inverse-mode');
        } else {
            $('#content').removeClass('content-inverse-mode');
        }
    });
};

var MapGoogle = function () {
    "use strict";
    return {
        //main function
        init: function () {
            handleGoogleMapSetting();
        }
    };
}();