package models.device

import java.util.UUID

import org.joda.time.DateTime
import utils.converters._

import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gbmetzner on 22/11/14.
 */
case class Device(id: Option[UUID],
                  code: String,
                  androidId: String,
                  description: Option[String],
                  createdAt: Option[DateTime] = None,
                  updatedAt: Option[DateTime] = None)

class DeviceTable(tag: Tag) extends Table[Device](tag, "device") {
  def id = column[Option[UUID]]("id", O.PrimaryKey)

  def code = column[String]("code", O.NotNull)

  def androidId = column[String]("androidid", O.NotNull)

  def description = column[Option[String]]("description")

  def createdAt = column[Option[DateTime]]("createdat")

  def updatedAt = column[Option[DateTime]]("updatedat")

  def * = (id, code, androidId, description, createdAt, updatedAt) <>(Device.tupled, Device.unapply)
}