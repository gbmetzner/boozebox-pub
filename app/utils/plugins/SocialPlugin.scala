package utils.plugins

import models.User
import play.api.Play.current
import play.api.libs.ws.WS
import play.api.{Application, Plugin}
import utils.AppConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.matching.Regex

/**
 * Created by gbmetzner on 17/06/14.
 */

trait SocialAPI {
  def login(code: String): Future[String]

  def user(code: String): Future[User]

  def post(message: String, code: String): Future[Boolean]
}

object Social {
  private def facebookAPI(implicit app: Application): SocialAPI = {
    app.plugin[SocialPlugin] match {
      case Some(plugin) => plugin.api
      case None => throw new Exception("There is no cache plugin registered. Make sure at least one SocialPlugin implementation is enabled.")
    }
  }

  def login(code: String): Future[String] = facebookAPI login code

  def user(code: String): Future[User] = facebookAPI user code

  def post(message: String, code: String): Future[Boolean] = facebookAPI post(message, code)

}

abstract class SocialPlugin extends Plugin {

  /**
   * Implementation of the the Social plugin
   * provided by this plugin.
   */
  def api: SocialAPI

}

class FacebookPlugin(app: Application) extends SocialPlugin {

  override def onStart() {

  }

  override def onStop() {

  }

  lazy val api = new FacebookImpl

}

class FacebookImpl extends SocialAPI {
  override def post(message: String, code: String): Future[Boolean] = {
    val replacedMessage = message.replaceAllLiterally(" ", "+")
    val replacedCaption = AppConfig.facebookPostCaption.replaceAllLiterally(" ", "+")
    val replacedDescription = AppConfig.facebookPostDescription.replaceAllLiterally(" ", "+")
    val replacedPicture = "https://s3-sa-east-1.amazonaws.com/bbcampaign/drinks/img/android_cocktail_alexander_young.png"//AppConfig.s3LinkDrink.replaceAllLiterally(" ", "+")
    val replacedName = AppConfig.facebookPostName.replaceAllLiterally(" ", "+")

    WS.url(s"${AppConfig.facebookPostUrl}?message=$replacedMessage&caption=$replacedCaption&description=$replacedDescription&picture=$replacedPicture&name=$replacedName&access_token=$code").post("").map {
      response => response.status == 200
    }
  }

  override def login(code: String): Future[String] = {

    val regex = new Regex("access_token=(.*)&expires=(.*)")

    def accessTokenFromBody(body: String) = body match {
      case regex(accessToken, expires) => accessToken
      case _ => ""
    }

    WS.url(s"${AppConfig.facebookAccessTokenUrl}?client_id=${AppConfig.facebookClientId}&redirect_uri=${AppConfig.facebookRedirectUrl}&client_secret=${AppConfig.facebookClientSecret}&code=${code}").get()
      .map { response =>
      accessTokenFromBody(response.body)
    }
  }

  override def user(code: String): Future[User] = WS.url(s"${AppConfig.facebookGetMetUrl}?access_token=${code}").get().map {
    response => User.fromJson(response.json)
  }
}
