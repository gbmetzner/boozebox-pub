package services

import models.drinktype.Type

/**
 * Created by gbmetzner on 09/08/14.
 */
trait TypeServiceComponent {

  def typeService: TypeService

  trait TypeService extends ServiceComponent[Type]
}
