package repositories

import models.admin.BoozeUser
import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gbmetzner on 08/11/14.
 */
trait BoozeUserRepositoryComponent {

  def boozeUserRepository: BoozeUserRepository

  trait BoozeUserRepository extends Repository[BoozeUser] {
    def authenticate(username: String, password: String)(implicit session: Session): Option[BoozeUser]
  }

}
