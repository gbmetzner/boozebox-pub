package models

import org.joda.time.{LocalDate, Years}
import play.api.libs.json._
import utils.converters._

/**
 * Created by gbmetzner on 21/06/14.
 */
case class User(fullName: String, gender: Option[String] = None, email: Option[String] = None, birthday: Option[LocalDate] = None) {
  def age: Option[Int] = birthday match {
    case Some(date) => Some(Years.yearsBetween(LocalDate.now, date).getYears)
    case None => None
  }

  def toJson = Json.stringify(toJsonValue)

  private def toJsonValue: JsValue = {
    Json.obj(
      "fullName" -> fullName,
      "gender" -> gender,
      "email" -> email,
      "age" -> age,
      "birthday" -> birthday)
  }
}

object User {
  def fromJson(json: JsValue): User = {
    User(fullName = (json \ "name").as[String],
      gender = (json \ "gender").asOpt[String],
      email = (json \ "email").asOpt[String],
      birthday = (json \ "birthday").asOpt[LocalDate])
  }
}