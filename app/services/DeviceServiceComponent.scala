package services

import models.device.Device

/**
 * Created by gbmetzner on 23/11/14.
 */
trait DeviceServiceComponent {

  def deviceService: DeviceService

  trait DeviceService extends ServiceComponent[Device] {
    def findByCode(code: String): Option[Device]

    def online(code: String): Boolean
  }

}
