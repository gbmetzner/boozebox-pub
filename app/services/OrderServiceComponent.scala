package services

import com.typesafe.scalalogging.LazyLogging
import models.Order
import models.dto.{RankedBooze, WebOrder => OrderView}

/**
 * Created by gbmetzner on 05/06/14.
 */
trait OrderServiceComponent {

  def orderService: OrderService

  trait OrderService extends ServiceComponent[Order] {

    def send(order: OrderView, accessToken: Option[String]): Unit

    def logOrder(logOrder: Order): String

    def logOrders(logOrders: List[Order]): String

    def latestOrders(quantity: Int): List[Order]

    def listRankedBooze(): List[RankedBooze]

    def statsByYear(): (Int, Double)

    def statsByMonth(): (Int, Double)

    def statsByWeek(): (Int, Double)

    def statsByDay(): (Int, Double)
  }

}
