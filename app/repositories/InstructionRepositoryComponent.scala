package repositories

import models.instruction.Instruction

/**
 * Created by gbmetzner on 19/09/14.
 */
trait InstructionRepositoryComponent {

  def instructionRepository: InstructionRepository

  trait InstructionRepository extends Repository[Instruction]

}

