package models.instruction

import java.util.UUID

import org.joda.time.DateTime
import utils.converters._
import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gbmetzner on 19/09/14.
 */
case class Instruction(id: Option[UUID],
                       read: String,
                       speak: String,
                       createdAt: Option[DateTime] = None,
                       updatedAt: Option[DateTime] = None) {

}

class InstructionTable(tag: Tag) extends Table[Instruction](tag, "instruction") {
  def id = column[Option[UUID]]("id", O.PrimaryKey)

  def read = column[String]("read", O.NotNull)

  def speak = column[String]("speak")

  def createdAt = column[Option[DateTime]]("createdat")

  def updatedAt = column[Option[DateTime]]("updatedat")

  def * = (id, read, speak, createdAt, updatedAt) <>(Instruction.tupled, Instruction.unapply)

}