package models.dto

import models.Order
import org.joda.time.DateTime
import utils.uuid._
import utils.converters.fmtFullDateTime

/**
 * Created by gbmetzner on 3/9/15.
 */
case class WSOrder(orderId: String,
                       owner: String,
                       boozeCode: String,
                       boozeName: String,
                       voucher: String,
                       inputTime: String,
                       startTime: String,
                       finishTime: String,
                       machine: String) {
  def toOrder: Order = {
    Order(Some(uuid), orderId.toLong, owner, boozeCode.toInt, boozeName, voucher, Option(DateTime.parse(inputTime, fmtFullDateTime)),
      DateTime.parse(startTime, fmtFullDateTime), DateTime.parse(finishTime, fmtFullDateTime), machine)
  }
}
