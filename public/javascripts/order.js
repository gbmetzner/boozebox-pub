/**
 * Created by gbmetzner on 06/02/15.
 */


$(document).ready(function () {

    $.getJSON("/order/booze/select", function (data) {
        $('#boozes').ddslick({
            data: data,
            selectText: "Escolha um coquetel:",
            imagePosition: "left",
            onSelected: function (selectedData) {
            }
        });
    });

    $.getJSON("/fb/logged", function (data) {

        $("#orderForm").append('<input id="isLogged" type="hidden" value=' + data.isLogged + ' />');

        if (data.isLogged) {
            //$("#fb-btn-div").append('<input id="fblogout" type="image" src="assets/images/facebook/face-logout.png" alt="Logout Facebook" />');
            $("#div-btn").append('<button name="button-submit" type="button" id="button-submit" >OK</button>');
        } else {
            //$("#fb-btn-div").append('<input id="fblogin" type="image" src="assets/images/facebook/face-login.png" alt="Login Facebook" />');
            $("#div-btn").append('<button name="submit-nv" type="button" id="submit-nv" >OK</button>');
        }
    }).done(function () {
        $("#submit-nv").click(function(e) {

            e.preventDefault();

            var data = JSON.stringify({
                name: $('#name').val(),
                voucher: $('#voucher').val(),
                cocktailId: $('.dd-selected-value').val(),
                postOnFB: $.parseJSON($('#isLogged').val())
            });

            $.ajax({
                type: 'POST',
                data: data,
                url: '/order',
                dataType: 'json',
                headers: {'Content-Type': 'application/json'},
                success: function (data) {
                    window.location.href = '/order/done?message=';
                },
                error: function (jqXHR, status, error) {

                        var jsonValue = $.parseJSON(jqXHR.responseText).message;

                        var text = '<ui>';

                        $.each(jsonValue, function(index, value) {
                            text += '<li>' + value + '</li>';
                        });

                        text += '</ui>';

                        $("#idContainer").empty();
                        $("#idContainer").append('<div id="msg" name="msg" role="alert" class="hidden"></div>');
                        $("#msg").removeClass("hidden");
                        $("#msg").addClass("alert alert-danger alert-dismissible");
                        $("#msg").append('<button id="close-btn" name="close-btn" type="button" class="close" data-dismiss="alert" aria-label="Close">');
                        $("#msg #close-btn").append('<span aria-hidden="true">&times;</span>');
                        $("#msg").append(text);
                }
            });
        });
    });
});