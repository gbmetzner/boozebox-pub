package repositories.impl

import java.util.UUID

import models.cup.{Glass, GlassTable}
import org.joda.time.DateTime
import repositories.GlassRepositoryComponent
import utils.paginate.Page
import utils.uuid._

import scala.slick.driver.PostgresDriver.simple._
import scala.slick.lifted.TableQuery

/**
 * Created by gbmetzner on 10/08/14.
 */
trait GlassRepositoryComponentImpl extends GlassRepositoryComponent {

  override def glassRepository: GlassRepository = new GlassRepositoryImpl

  class GlassRepositoryImpl extends GlassRepository {

    private val tableQuery = TableQuery[GlassTable]

    override def save(glass: Glass)(implicit s: Session): Unit = {
      val now = Some(DateTime.now)
      tableQuery += glass.copy(id = Some(uuid), createdAt = now, updatedAt = now)
    }

    override def update(glass: Glass)(implicit s: Session): Unit = {
      tableQuery.filter(_.id === glass.id)
        .map(c => (c.name, c.size, c.updatedAt))
        .update((glass.name, glass.size, Some(DateTime.now())))
    }

    override def findById(id: UUID)(implicit s: Session): Option[Glass] = tableQuery filter (_.id === id) firstOption

    override def list(page: Int, length: Int, query: Option[String] = None)(implicit s: Session): Page[Glass] = {

      val filtered = query match {
        case Some(query) => tableQuery.filter(_.name.toLowerCase like s"%${query.toLowerCase}%")
        case None => tableQuery
      }

      val total = filtered.length.run

      Page(filtered.drop(page).take(length).list, page, page, total)
    }

    override def listAll()(implicit s: Session): List[Glass] = tableQuery list


    override def delete(id: UUID)(implicit s: Session): Unit = tableQuery filter (_.id === id) delete


  }

}
