package controllers

import models.dto.WebOrder
import play.api.Play.current
import play.api.cache.Cached
import play.api.i18n.Messages
import play.api.libs.json.Json
import play.api.mvc.{Action, BodyParsers, Controller}
import services.registry.OrderRegistry
import services.{BoozeServiceComponent, OrderServiceComponent}
import views.html

/**
 * Created by gbmetzner on 05/06/14.
 */
object OrderController extends OrderController with OrderRegistry

trait OrderController extends Controller {
  self: OrderServiceComponent with BoozeServiceComponent =>

  def form = Action {
    Ok(html.order.order())
  }

  def boozesSelect = Action {

    import utils.parsers.boozeSelectWrites

    Ok(Json.toJson(boozeService.listAll))
  }


  def fbLogged = Action {
    request =>
      Ok(Json.obj("isLogged" -> request.session.get("access_token").isDefined))
  }

  def submit = Action(BodyParsers.parse.json) {
    request =>

      import utils.parsers.webOrderReads

      request.body.validate[WebOrder].fold(
        errors => {

          val names = errors.map(j => j._1.toJsonString.split("\\.").last)

          val messages = names.map(name => Json.toJson(Messages(s"$name.required")))

          BadRequest(Json.obj("message" -> messages))
        },
        order => {

          val accessToken = if (order.postOnFB) request.session.get("access_token") else None

          orderService send(order, accessToken)

          Ok(Json.obj("success" -> order.cocktailId))
        }
      )
  }

  def done = Action {
    request =>

      Ok(html.order.done(request.getQueryString("message").getOrElse("")))
  }
}