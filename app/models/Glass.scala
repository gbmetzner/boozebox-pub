package models.cup

import java.util.UUID

import org.joda.time.DateTime
import utils.AppConfig
import utils.converters._

import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gbmetzner on 10/08/14.
 */
case class Glass(id: Option[UUID],
               name: String,
               size: String,
               createdAt: Option[DateTime] = None,
               updatedAt: Option[DateTime] = None) {

  def imageFull: String = id match {
    case Some(img) => s"${AppConfig.s3Path}${AppConfig.s3CupUploadKey}$img.png"
    case None => s"${AppConfig.s3Path}${AppConfig.s3CupUploadKey}default.png"
  }

}

class GlassTable(tag: Tag) extends Table[Glass](tag, "glass") {
  def id = column[Option[UUID]]("id", O.PrimaryKey)

  def name = column[String]("name", O.NotNull)

  def size = column[String]("size")

  def createdAt = column[Option[DateTime]]("createdat")

  def updatedAt = column[Option[DateTime]]("updatedat")

  def * = (id, name, size, createdAt, updatedAt) <>(Glass.tupled, Glass.unapply)

}