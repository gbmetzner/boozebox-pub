package models.ingredient

import java.util.UUID

import org.joda.time.DateTime
import utils.AppConfig
import utils.converters._

import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gbmetzner on 17/08/14.
 */
case class Ingredient(id: Option[UUID],
                      ingredient: String,
                      density: Option[Double],
                      createdAt: Option[DateTime] = None,
                      updatedAt: Option[DateTime] = None) {
  def imageFull: String = id match {
    case Some(img) => s"${AppConfig.s3Path}${AppConfig.s3IngredientUploadKey}$img.png"
    case None => s"${AppConfig.s3Path}${AppConfig.s3IngredientUploadKey}default.png"
  }
}

class IngredientTable(tag: Tag) extends Table[Ingredient](tag, "ingredient") {
  def id = column[Option[UUID]]("id", O.PrimaryKey)

  def ingredient = column[String]("ingredient", O.NotNull)

  def density = column[Option[Double]]("density")

  def createdAt = column[Option[DateTime]]("createdat")

  def updatedAt = column[Option[DateTime]]("updatedat")

  def * = (id, ingredient, density, createdAt, updatedAt) <>(Ingredient.tupled, Ingredient.unapply)

}
