package controllers.admin

import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import services.OrderServiceComponent
import services.registry.OrderRegistry
import views.html
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * Created by gbmetzner on 2/20/15.
 */
object DashboardController extends DashboardController with OrderRegistry

trait DashboardController extends Controller {
  requires: OrderServiceComponent =>

  def dashboard = Action {
    Ok(html.admin.dashboard.dashboard())
  }

  def statsByYear = Action.async {
    Future {
      val (countByYear, percentIncrease) = orderService.statsByYear()
      val statsByYear = Json.obj("countByYear" -> countByYear,
        "percentIncrease" -> percentIncrease.formatted("%.2f"),
        "percentIncreaseMsg" -> s"Better than last year (${percentIncrease.formatted("%.2f")}%)")
      Ok(statsByYear)
    }
  }

  def statsByMonth = Action.async {
    Future {
      val (countByMonth, percentIncrease) = orderService.statsByMonth()
      val statsByMonth = Json.obj("countByMonth" -> countByMonth,
        "percentIncrease" -> percentIncrease.formatted("%.2f"),
        "percentIncreaseMsg" -> s"Better than last month (${percentIncrease.formatted("%.2f")}%)")
      Ok(statsByMonth)
    }
  }

  def statsByWeek = Action.async {
    Future {
      val (countByWeek, percentIncrease) = orderService.statsByWeek()
      val statsByWeek = Json.obj("countByWeek" -> countByWeek,
        "percentIncrease" -> percentIncrease.formatted("%.2f"),
        "percentIncreaseMsg" -> s"Better than last week (${percentIncrease.formatted("%.2f")}%)")
      Ok(statsByWeek)
    }
  }

  def statsByDay = Action.async {
    Future {
      val (countByDay, percentIncrease) = orderService.statsByDay()
      val statsByDay = Json.obj("countByDay" -> countByDay,
        "percentIncrease" -> percentIncrease.formatted("%.2f"),
        "percentIncreaseMsg" -> s"Better than last day (${percentIncrease.formatted("%.2f")}%)")
      Ok(statsByDay)
    }
  }

  def latestOrders(quantity: Int) = Action.async {
    import utils.parsers.latestOrdersWrites
    Future {
      val latestOrders = orderService latestOrders quantity
      Ok(Json toJson latestOrders)
    }
  }
  
  def listRankedBooze = Action.async {
    import utils.parsers.rankedBoozeWrites
    Future {
      val ranked = orderService.listRankedBooze()
      Ok(Json toJson ranked)
    }
  }

}