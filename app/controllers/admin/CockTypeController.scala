package controllers.admin

import java.util.UUID

import models.cocktype.BoozeType
import play.api.libs.json.{JsError, Json}
import play.api.mvc.{Action, BodyParsers}
import services.BoozeTypeServiceComponent
import services.registry.CockTypeRegistry
import views.html

/**
 * Created by gbmetzner on 23/09/14.
 */
object CockTypeController extends CockTypeController with CockTypeRegistry

trait CockTypeController extends BaseController {
  self: BoozeTypeServiceComponent =>

  import utils.parsers.boozeTypeFormat

  def form = Action {
    Ok(html.admin.cocktype.add())
  }

  def save = Action(BodyParsers.parse.json) {
    implicit request =>

      logger info s"request: ${request.body.toString}"

      request.body.validate[BoozeType].fold(
        errors => BadRequest(Json.obj("status" -> "400", "message" -> JsError.toFlatJson(errors))),
        cockType => {
          boozeTypeService persist cockType
          Ok(Json.obj("status" -> "OK", "message" -> s"CockType: ${cockType.id} saved."))
        }
      )
  }

  def update(id: UUID) = Action(BodyParsers.parse.json) {
    implicit request =>

      request.body.validate[BoozeType].fold(
        errors => {
          BadRequest(Json.obj("status" -> "400", "message" -> JsError.toFlatJson(errors)))
        },
        cockType => {
          boozeTypeService persist cockType
          Ok(Json.obj("status" -> "OK", "message" -> s"CockType: ${cockType.id} updated."))
        }
      )
  }

  /**
   * Find the CockType
   *
   * @param id Current id of the CockType to be updated.
   */
  def edit(id: UUID) = Action {
    implicit request =>
      boozeTypeService.findById(id) match {
        case Some(cockType) => Ok(html.admin.cocktype.edit(cockType))
        case None => NotFound
      }
  }

  /**
   * Display the page list of CockType.
   */
  def listPage = Action {
    implicit request =>
      logger info "listing cocktypes..."
      Ok(html.admin.cocktype.list())
  }

  /**
   * Display the list of CockType by page.
   */
  def pagination = Action {
    implicit request =>

      val (draw, start, length, query) = parseQueryStringPagination(request.queryString)

      logger info (query.toString)

      logger info s"start:$start length:$length query:$query"

      val cockTypes = boozeTypeService list(start, length, query)

      val cockTypesJson = cockTypes.items.map(c => Json.toJson(c))

      Ok(Json.obj("draw" -> draw, "recordsTotal" -> cockTypes.total, "recordsFiltered" -> cockTypes.total, "data" -> cockTypesJson))
  }

  /**
   * Delete the CockType
   *
   * @param id Current id of the CockType to be deleted.
   */
  def delete(id: UUID) = Action {
    implicit request =>
      boozeTypeService delete id
      Ok(Json.obj("message" -> "CockType was deleted."))
  }

}
