package controllers.admin

import java.util.UUID

import models.device.Device
import play.api.libs.json.{JsError, Json}
import play.api.mvc.{Action, BodyParsers}
import services.DeviceServiceComponent
import services.registry.DeviceRegistry
import views.html

/**
 * Created by gbmetzner on 23/11/14.
 */
object DeviceController extends DeviceController with DeviceRegistry

trait DeviceController extends BaseController {
  self: DeviceServiceComponent =>

  import utils.parsers.deviceFormat

  def form = Action {
    Ok(html.admin.device.add())
  }

  def save = Action(BodyParsers.parse.json) {
    implicit request =>

      logger info s"save: ${request.body.toString}"

      request.body.validate[Device].fold(
        errors => BadRequest(Json.obj("status" -> "400", "message" -> JsError.toFlatJson(errors))),
        device => {
          deviceService persist device
          Ok(Json.obj("status" -> "OK", "message" -> s"Device: ${device.description} saved."))
        }
      )
  }

  def update(id: UUID) = Action(BodyParsers.parse.json) {
    implicit request =>

      logger info s"update: ${request.body.toString()}"

      request.body.validate[Device].fold(
        errors => BadRequest(Json.obj("status" -> "400", "message" -> JsError.toFlatJson(errors))),
        device => {
          deviceService persist device
          Ok(Json.obj("status" -> "OK", "message" -> s"Device: ${device.description} updated."))
        }
      )
  }

  /**
   * Find the Device
   *
   * @param id Current id of the Device to be updated.
   */
  def edit(id: UUID) = Action {
    implicit request =>
      deviceService findById id match {
        case Some(device) => Ok(html.admin.device.edit(device))
        case None => NotFound
      }
  }

  /**
   * Display the page list of Devices.
   */
  def listPage = Action {
    implicit request =>
      logger info "listing devices..."
      Ok(html.admin.device.list())
  }

  /**
   * Display the list of Device by page.
   */
  def pagination = Action {
    implicit request =>

      val (draw, start, length, query) = parseQueryStringPagination(request.queryString)

      logger info s"start:$start length:$length query:$query"

      val devices = deviceService list(start, length, query)

      val devicesJson = devices.items.map(d => Json.toJson(d))

      Ok(Json.obj("draw" -> draw, "recordsTotal" -> devices.total, "recordsFiltered" -> devices.total, "data" -> devicesJson))
  }

  /**
   * Delete the Device
   *
   * @param id Current id of the Device to be deleted.
   */
  def delete(id: UUID) = Action {
    implicit request =>
      deviceService delete id
      Ok(Json.obj("message" -> "Device was deleted."))
  }

}
