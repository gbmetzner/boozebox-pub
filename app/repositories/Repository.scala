package repositories

import java.util.UUID

import utils.paginate.Page

import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gbmetzner on 17/08/14.
 */
trait Repository[T] {

  def save(t: T)(implicit s: Session)

  def list(page: Int, length: Int, query: Option[String] = None)(implicit s: Session): Page[T]

  def listAll()(implicit s: Session): List[T]

  def findById(id: UUID)(implicit s: Session): Option[T]

  def update(t: T)(implicit s: Session): Unit

  def delete(id: UUID)(implicit s: Session): Unit
}
