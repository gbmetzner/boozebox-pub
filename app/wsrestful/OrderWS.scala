package wsrestful

import models.Order
import models.dto.WSOrder
import play.api.libs.json.{Json, Writes}
import play.api.mvc.{Action, Controller}
import services.OrderServiceComponent
import services.registry.OrderRegistry

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * Created by gbmetzner on 10/02/15.
 */
object OrderWS extends OrderWS with OrderRegistry

trait OrderWS extends Controller {
  self: OrderServiceComponent =>

  def registerOrders = Action.async(parse.json) {
    request =>

      Future {

        import utils.parsers.mobileOrderReads

        request.body.validate[List[WSOrder]].fold(
          errors => BadRequest(errors.toString()),
          mobileOrders => {

            import actors.OrderActor._

            orderActor ! LogOrder(mobileOrders.map(_.toOrder))

            Ok(mobileOrders.head.machine)
          })
      }
  }
}
