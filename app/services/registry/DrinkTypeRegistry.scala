package services.registry

import repositories.impl.TypeRepositoryComponentImpl
import services.impl.TypeServiceComponentImpl

/**
 * Created by gbmetzner on 09/08/14.
 */
trait DrinkTypeRegistry extends TypeServiceComponentImpl with TypeRepositoryComponentImpl
