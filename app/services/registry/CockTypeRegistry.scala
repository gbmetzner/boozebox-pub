package services.registry

import repositories.impl.BoozeTypeRepositoryComponentImpl
import services.impl.BoozeTypeServiceComponentImpl

/**
 * Created by gbmetzner on 23/09/14.
 */
trait CockTypeRegistry extends BoozeTypeServiceComponentImpl with BoozeTypeRepositoryComponentImpl
