$(document).ready(function () {

    var idSelected = $('#cupId').val();

    $.getJSON("/admin/cup/listAll", function (data) {
        $.each(data, function (index, value) {

            if(value.id == idSelected){
                $('#cups').append($('<option selected />').val(value.id).text(value.name + ' - ' + value.size));
            }else{
                $('#cups').append($('<option />').val(value.id).text(value.name + ' - ' + value.size));
            }

        });
    });

    $('#cocktailForm').bootstrapValidator().on('success.form.bv', function (e) {

        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        var data = JSON.stringify({
            idCup: $('#cups').val(),
            deviceCode: parseInt($('#devcode').val()),
            name: $('#name').val(),
            color: $('#color').val(),
            rgb: $('#rgb').val(),
            pdvCode: parseInt($('#pdvcode').val()),
            author: $('#author').val()
        });

        $.ajax({
            type: 'PUT',
            data: data,
            url: $form.attr('action'),
            dataType: 'json',
            headers: { 'Content-Type': 'application/json' },
            success: function (data) {
                $("#dialog").children().empty();
                $("#dialog").children().append(data.message);
                $("#dialog").dialog({
                    modal: true,
                    closeOnEscape: false,
                    dialogClass: 'noclose',
                    buttons: {
                        Ok: function () {
                            window.location.href = '/admin/cocktail/list';
                        }
                    }
                });
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                $("#dialog").children().empty();
                $("#dialog").children().append(err.message);
                $("#dialog").dialog({
                    modal: true,
                    closeOnEscape: false,
                    dialogClass: 'noclose',
                    buttons: {
                        Ok: function () {
                            $("#dialog").dialog("close");
                        }
                    }
                });
            }
        });
    });

});