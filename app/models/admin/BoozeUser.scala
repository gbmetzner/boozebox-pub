package models.admin

import java.util.UUID

import org.joda.time.DateTime
import utils.converters._

import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gbmetzner on 31/07/14.
 */
case class BoozeUser(id: Option[UUID], name: String, username: String, password: String,
                createdOn: Option[DateTime], updatedOn: Option[DateTime])

class BoozeUserTable(tag: Tag) extends Table[BoozeUser](tag, "user") {
  def id = column[Option[UUID]]("id", O.PrimaryKey)

  def name = column[String]("name", O.NotNull)

  def username = column[String]("username", O.NotNull)

  def password = column[String]("password", O.NotNull)

  def createdOn = column[Option[DateTime]]("created_on", O.NotNull)

  def updatedOn = column[Option[DateTime]]("updated_on", O.NotNull)

  def * = (id, name, username, password, createdOn, updatedOn) <>(BoozeUser.tupled, BoozeUser.unapply)

}

