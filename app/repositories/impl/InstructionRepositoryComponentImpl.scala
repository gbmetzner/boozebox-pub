package repositories.impl

import java.util.UUID

import models.instruction.{Instruction, InstructionTable}
import org.joda.time.DateTime
import repositories.InstructionRepositoryComponent
import utils.uuid._
import utils.paginate.Page

import scala.slick.driver.PostgresDriver.simple._
import scala.slick.lifted.TableQuery

/**
 * Created by gbmetzner on 19/09/14.
 */
trait InstructionRepositoryComponentImpl extends InstructionRepositoryComponent {

  override def instructionRepository: InstructionRepository = new InstructionRepositoryImpl

  class InstructionRepositoryImpl extends InstructionRepository {

    private val tableQuery = TableQuery[InstructionTable]

    override def save(instruction: Instruction)(implicit s: Session): Unit = {
      val now = Some(DateTime.now)
      tableQuery += instruction.copy(id = Some(uuid), createdAt = now, updatedAt = now)
    }

    override def update(instruction: Instruction)(implicit s: Session): Unit = {
      tableQuery.filter(_.id === instruction.id)
        .map(i => (i.read, i.speak, i.updatedAt))
        .update((instruction.read, instruction.speak, Some(DateTime.now)))

    }

    override def findById(id: UUID)(implicit s: Session): Option[Instruction] = {
      tableQuery.filter(_.id === id).firstOption
    }

    override def delete(id: UUID)(implicit s: Session): Unit = {
      tableQuery.filter(_.id === id).delete
    }

    override def list(page: Int, length: Int, query: Option[String] = None)(implicit s: Session): Page[Instruction] = {

      val ingredients = tableQuery.drop(page).take(length).list

      val total = tableQuery.length.run

      Page(ingredients, page, page, total)
    }

    override def listAll()(implicit s: Session): List[Instruction] = tableQuery.list

  }

}
