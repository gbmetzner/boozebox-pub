package controllers.admin

import java.io.File
import java.util.UUID

import models.booze.Booze
import play.api.cache.Cache
import play.api.libs.json.{JsError, Json}
import play.api.mvc.{Action, BodyParsers}
import services.registry.BoozeRegistry
import services.{BoozeServiceComponent, GlassServiceComponent, UploadServiceComponent}
import utils.AppConfig._
import views.html
import play.api.Play.current

/**
 * Created by gbmetzner on 24/08/14.
 */
object BoozeController extends BoozeController with BoozeRegistry

trait BoozeController extends BaseController {
  self: BoozeServiceComponent with GlassServiceComponent with UploadServiceComponent =>

  import utils.parsers.boozeFormat

  /**
   * Display the page to add Cocktail.
   */
  def form = Action {
    Ok(html.admin.booze.add())
  }


  /**
   * Create Cocktail.
   */
  def save = Action(BodyParsers.parse.json) {
    implicit request =>

      logger info s"request: ${request.body.toString}"

      request.body.validate[Booze].fold(
        errors => BadRequest(Json.obj("status" -> "400", "message" -> JsError.toFlatJson(errors))),
        cocktail => {
          boozeService persist cocktail
          Cache.remove("cocktails")
          Ok(Json.obj("status" -> "OK", "message" -> s"Cocktail: ${cocktail.name} saved."))
        }
      )
  }

  /**
   * Display the page list of Cocktails.
   */
  def listPage = Action {
    implicit request =>
      Ok(html.admin.booze.list())
  }

  /**
   * Display the list of Cocktails1 by page.
   */
  def pagination = Action {
    implicit request =>

      val (draw, start, length, query) = parseQueryStringPagination(request.queryString)

      logger info (query.toString)

      val cocktails = boozeService list(start, length, query)

      val cocktailsJson = cocktails.items.map(c => Json.toJson(c))

      Ok(Json.obj("draw" -> draw, "recordsTotal" -> cocktails.total, "recordsFiltered" -> cocktails.total, "data" -> cocktailsJson))
  }

  /**
   * Find the Cocktail
   *
   * @param id Current id of the Cocktail to be updated
   */
  def edit(id: UUID) = Action {
    implicit request =>
      boozeService findById id match {
        case Some(cocktail) => Ok(html.admin.booze.edit(cocktail))
        case None => NotFound
      }
  }


  /**
   * Upload the Cocktails Image.
   */
  def upload = Action(parse.multipartFormData) {
    implicit request =>

      request.body.file("image").map {
        image =>

          val filename = image.filename
          val file = new File(s"/tmp/$filename")

          image.ref.moveTo(file)

          uploadService upload(s3Bucket, s"$s3CocktailUploadKey${filename}", file)

          file.delete

          Ok("Ok")
      }.getOrElse {
        NotFound
      }
  }

  /**
   * Update Cocktail.
   * @param id Current id of the Cocktail to be updated
   */
  def update(id: UUID) = Action(BodyParsers.parse.json) {
    implicit request =>

      request.body.validate[Booze].fold(
        errors => {
          BadRequest(Json.obj("status" -> "400", "message" -> JsError.toFlatJson(errors)))
        },
        cocktail => {
          println(cocktail)
          boozeService persist (cocktail)
          Ok(Json.obj("status" -> "OK", "message" -> s"Cup: ${cocktail.name} updated."))
        }
      )
  }


  /**
   * Delete the Cocktail
   *
   * @param id Current id of the Cocktail to be deleted.
   */
  def delete(id: UUID) = Action {
    implicit request =>
      boozeService delete id
      Ok(Json.obj("message" -> "Cocktail was deleted."))
  }

  /**
   * return all cocktails.
   */
  def all = Action {
    Ok(Json toJson (boozeService listAll))
  }

}
