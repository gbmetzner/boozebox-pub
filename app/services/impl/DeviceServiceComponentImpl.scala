package services.impl

import java.util.UUID

import models.device.Device
import repositories.DeviceRepositoryComponent
import services.{AndroidMessengerComponent, DeviceServiceComponent}
import utils.paginate.Page

/**
 * Created by gbmetzner on 23/11/14.
 */
trait DeviceServiceComponentImpl extends DeviceServiceComponent {
  self: DeviceRepositoryComponent with AndroidMessengerComponent =>

  override def deviceService: DeviceService = new DeviceServiceImpl

  class DeviceServiceImpl extends DeviceService {
    override def persist(device: Device): Unit = db withTransaction {
      implicit session =>
        device.id match {
          case Some(_) => deviceRepository update device
          case None => deviceRepository save device
        }

    }

    override def findById(id: UUID): Option[Device] = db withSession {
      implicit session =>
        deviceRepository findById id
    }

    override def delete(id: UUID): Unit = db withTransaction {
      implicit session =>
        deviceRepository delete id
    }

    override def list(page: Int, length: Int, query: Option[String]): Page[Device] = db withSession {
      implicit session =>
        deviceRepository list(page, length, query)
    }

    override def listAll: List[Device] = db withSession {
      implicit session =>
        deviceRepository listAll()
    }

    override def findByCode(code: String): Option[Device] = db withSession {
      implicit session =>
        deviceRepository findByCode code
    }

    override def online(code: String): Boolean = {
      findByCode(code) match {
        case Some(c) => androidMessenger sendMessage(code, "msg" -> "isOnline")
        case None => false
      }
    }
  }

}
