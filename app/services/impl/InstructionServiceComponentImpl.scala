package services.impl

import java.util.UUID

import models.instruction.Instruction
import repositories.InstructionRepositoryComponent
import services.{DBService, InstructionServiceComponent}
import utils.paginate.Page

/**
 * Created by gbmetzner on 19/09/14.
 */
trait InstructionServiceComponentImpl extends InstructionServiceComponent {
  self: InstructionRepositoryComponent =>

  override def instructionService: InstructionService = new InstructionServiceImpl

  class InstructionServiceImpl extends InstructionService with DBService {

    override def persist(instruction: Instruction): Unit = db.withTransaction {
      implicit session =>
        instruction.id match {
          case Some(_) => instructionRepository update instruction
          case None => instructionRepository save instruction
        }
    }

    override def list(page: Int, length: Int, query: Option[String]): Page[Instruction] = db.withSession {
      implicit session =>
        instructionRepository list(page, length, query)
    }

    override def listAll: List[Instruction] = db.withSession {
      implicit session =>
        instructionRepository listAll()
    }

    override def findById(id: UUID): Option[Instruction] = db.withSession {
      implicit session =>
        instructionRepository findById id
    }

    override def delete(id: UUID): Unit = db.withTransaction {
      implicit session =>
        instructionRepository delete id
    }
  }

}
