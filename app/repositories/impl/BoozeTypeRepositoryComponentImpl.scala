package repositories.impl

import java.util.UUID

import models.cocktype.{BoozeType, BoozeTypeTable}
import org.joda.time.DateTime
import repositories.BoozeTypeRepositoryComponent
import utils.uuid._
import utils.paginate.Page

import scala.slick.driver.PostgresDriver.simple._
import scala.slick.lifted.TableQuery

/**
 * Created by gbmetzner on 20/09/14.
 */
trait BoozeTypeRepositoryComponentImpl extends BoozeTypeRepositoryComponent {

  override def boozeTypeRepository: BoozeTypeRepository = new BoozeTypeRepositoryImpl

  class BoozeTypeRepositoryImpl extends BoozeTypeRepository {

    private val tableQuery = TableQuery[BoozeTypeTable]

    override def save(cockType: BoozeType)(implicit s: Session): Unit = {
      val now = Some(DateTime.now)
      tableQuery += cockType.copy(id = Some(uuid), createdAt = now, updatedAt = now)
    }

    override def update(cockType: BoozeType)(implicit s: Session): Unit = {
      tableQuery.filter(_.id === cockType.id)
        .map(c => (c.typeId, c.cocktailId, c.updatedAt))
        .update((cockType.typeId, cockType.cocktailId, Some(DateTime.now)))
    }

    override def findById(id: UUID)(implicit s: Session): Option[BoozeType] = tableQuery.filter(_.id === id).firstOption

    override def delete(id: UUID)(implicit s: Session): Unit = tableQuery.filter(_.id === id).delete

    override def list(page: Int, length: Int, query: Option[String])(implicit s: Session): Page[BoozeType] = {

      val pageSize = 10
      val offset = pageSize * page
      val cups = tableQuery.drop(offset).take(pageSize).list

      val total = tableQuery.length.run

      Page(cups, page, offset, total)

    }

    override def listAll()(implicit s: Session): List[BoozeType] = tableQuery.list
  }

}
