package repositories

import models.booze.Booze

/**
 * Created by gbmetzner on 24/08/14.
 */
trait BoozeRepositoryComponent {

  def boozeRepository: BoozeRepository

  trait BoozeRepository extends Repository[Booze]

}
