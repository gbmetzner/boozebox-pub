package controllers.admin

import com.typesafe.scalalogging.{LazyLogging, Logger}
import org.slf4j.LoggerFactory
import play.api.mvc.Controller

/**
 * Created by gbmetzner on 31/08/14.
 */
trait BaseController extends Controller with LazyLogging {

  override protected lazy val logger: Logger = Logger(LoggerFactory.getLogger("controllers"))

  protected def parseQueryStringPagination(queryString: Map[String, Seq[String]]): (Int, Int, Int, Option[String]) = {
    val draw = queryString("draw").map(_.toInt).headOption.getOrElse(0)
    val start = queryString("start").map(_.toInt).headOption.getOrElse(0)
    val length = queryString("length").map(_.toInt).headOption.getOrElse(0)
    val query = queryString("search[value]").filter(_ != "").map(_.toString).headOption
    (draw, start, length, query)
  }
}
