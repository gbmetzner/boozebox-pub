package services

import models.ingredient.Ingredient

/**
 * Created by gbmetzner on 17/08/14.
 */
trait IngredientServiceComponent {

  def ingredientService: IngredientService

  trait IngredientService extends ServiceComponent[Ingredient]

}
