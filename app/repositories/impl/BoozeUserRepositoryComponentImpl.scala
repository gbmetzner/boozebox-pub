package repositories.impl

import java.util.UUID

import models.admin.{BoozeUser, BoozeUserTable}
import org.joda.time.DateTime
import repositories.BoozeUserRepositoryComponent
import utils.paginate.Page
import utils.uuid._
import scala.slick.driver.PostgresDriver.simple._
import scala.slick.lifted.TableQuery

/**
 * Created by gbmetzner on 08/11/14.
 */
trait BoozeUserRepositoryComponentImpl extends BoozeUserRepositoryComponent {

  override def boozeUserRepository: BoozeUserRepository = new BoozeUserRepositoryImpl

  class BoozeUserRepositoryImpl extends BoozeUserRepository {

    private val tableQuery = TableQuery[BoozeUserTable]

    override def save(user: BoozeUser)(implicit session: Session): Unit = {
      val now = Some(DateTime.now)
      tableQuery += user.copy(id = Some(uuid), createdOn = now, updatedOn = now)
    }

    override def update(user: BoozeUser)(implicit session: Session): Unit = {
      tableQuery.filter(_.id === user.id)
        .map(u => (u.name, u.username, u.updatedOn))
        .update((user.name, user.username, Some(DateTime.now)))
    }

    override def findById(id: UUID)(implicit session: Session): Option[BoozeUser] = {
      tableQuery.filter(_.id === id).firstOption
    }

    override def delete(id: UUID)(implicit session: Session): Unit = {
      tableQuery.filter(_.id === id).delete
    }

    override def list(page: Int, length: Int, query: Option[String])(implicit session: Session): Page[BoozeUser] = {

      val filtered = query match {
        case Some(q) => tableQuery.filter(_.name.toLowerCase like s"%${q.toLowerCase}%")
        case None => tableQuery
      }

      val total = filtered.length.run

      Page(filtered.drop(page).take(length).list, page, page, total)

    }

    override def listAll()(implicit session: Session): List[BoozeUser] = tableQuery.list

    override def authenticate(username: String, password: String)(implicit session: Session): Option[BoozeUser] = {
      tableQuery.filter(but => but.username === username && but.password === password).firstOption
    }
  }

}
