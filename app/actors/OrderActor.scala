package actors

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import models.Order
import services.OrderServiceComponent
import services.registry.OrderRegistry

/**
 * Created by gbmetzner on 2/13/15.
 */
object OrderActor {

  val system = ActorSystem("OrderSystem")
  val orderActor = system.actorOf(Props(OrderActor()), name = "OrderActor")

  case class LogOrder(orders: List[Order])

  def apply(): OrderActor = new OrderActor with OrderRegistry

}

class OrderActor extends Actor with ActorLogging {
  requires: OrderServiceComponent =>

  import actors.OrderActor._

  override def receive: Receive = {
    case LogOrder(orders) => orderService logOrders orders
  }
}
