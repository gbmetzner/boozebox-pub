package models.drinktype

import java.util.UUID

import org.joda.time.DateTime
import utils.converters._

import scala.slick.driver.PostgresDriver.simple._
import scala.slick.lifted.Tag

/**
 * Created by gbmetzner on 09/08/14.
 */
case class Type(id: Option[UUID],
                description: String,
                createdAt: Option[DateTime] = None,
                updatedAt: Option[DateTime] = None)

class TypeTable(tag: Tag) extends Table[Type](tag, "type") {
  def id = column[Option[UUID]]("id", O.PrimaryKey)

  def description = column[String]("description", O.NotNull)

  def createdAt = column[Option[DateTime]]("createdat")

  def updatedAt = column[Option[DateTime]]("updatedat")

  def * = (id, description, createdAt, updatedAt) <>(Type.tupled, Type.unapply)

}