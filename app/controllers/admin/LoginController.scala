package controllers.admin

import auth.BoozeBoxAuthConfig
import jp.t2v.lab.play2.auth.LoginLogout
import play.api.data.Forms._
import play.api.data._
import play.api.i18n.Messages
import play.api.libs.json.Json
import play.api.mvc.Action
import services.BoozeUserServiceComponent
import services.registry.BoozeUserRegistry
import views.html
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * Created by gbmetzner on 31/07/14.
 */
object LoginController extends LoginController with BoozeUserRegistry

trait LoginController extends BaseController with LoginLogout with BoozeBoxAuthConfig {
  requires: BoozeUserServiceComponent =>

  val loginForm = Form(
    tuple(
      "username" -> nonEmptyText,
      "password" -> nonEmptyText
    )
  )

  def login = Action {
    Ok(html.admin.login.form(loginForm))
  }


  def authenticate = Action.async {
    implicit request =>

      loginForm.bindFromRequest.fold(
        errors => Future.successful(BadRequest(html.admin.login.form(errors))),
        login => boozeUserService.authenticate(login._1, login._2) match {
          case Some(boozeUser) => gotoLoginSucceeded(boozeUser.id.get)
          case None => Future.successful(Unauthorized(Json.obj("warning" -> Messages("user.not.found"))))
        }
      )
  }
}


