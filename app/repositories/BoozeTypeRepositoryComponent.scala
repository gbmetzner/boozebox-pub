package repositories

import models.cocktype.BoozeType

/**
 * Created by gbmetzner on 20/09/14.
 */
trait BoozeTypeRepositoryComponent {

  def boozeTypeRepository: BoozeTypeRepository

  trait BoozeTypeRepository extends Repository[BoozeType]

}
