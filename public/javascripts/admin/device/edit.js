$(document).ready(function () {

    $('#deviceForm').bootstrapValidator().on('success.form.bv', function (e) {

        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        var data = JSON.stringify({
            code: $('#code').val(),
            androidId: $('#androidId').val(),
            description: $('#description').val()
        });

        $.ajax({
            type: 'PUT',
            data: data,
            url: $form.attr('action'),
            dataType: 'json',
            headers: { 'Content-Type': 'application/json' },
            success: function (data) {
                $("#dialog").children().empty();
                $("#dialog").children().append(data.message);
                $("#dialog").dialog({
                    modal: true,
                    closeOnEscape: false,
                    dialogClass: 'noclose',
                    buttons: {
                        Ok: function () {
                            window.location.href = '/admin/device/list';
                        }
                    }
                });
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                $("#dialog").children().empty();
                $("#dialog").children().append(err.message);
                $("#dialog").dialog({
                    modal: true,
                    closeOnEscape: false,
                    dialogClass: 'noclose',
                    buttons: {
                        Ok: function () {
                            $("#dialog").dialog("close");
                        }
                    }
                });
            }
        });
    });
});