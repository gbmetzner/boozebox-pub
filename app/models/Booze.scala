package models.booze

import java.util.UUID

import org.joda.time.DateTime
import utils.AppConfig._
import utils.converters._

import scala.language.postfixOps
import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gustavo on 5/26/14.
 */
case class Booze(id: Option[UUID],
                 cupId: UUID,
                 deviceCode: Int,
                 name: String,
                 color: Option[String],
                 rgb: Option[String],
                 pdvCode: Option[Int],
                 author: Option[String],
                 createdAt: Option[DateTime] = None,
                 updatedAt: Option[DateTime] = None) {

  def imageFull: String = id match {
    case Some(path) => s"$s3Path$s3CocktailUploadKey$path.png"
    case None => ""
  }
}

class BoozeTable(tag: Tag) extends Table[Booze](tag, "booze") {
  def id = column[Option[UUID]]("id", O.PrimaryKey)

  def cupId = column[UUID]("cupid", O.NotNull)

  def deviceCode = column[Int]("devicecode", O.NotNull)

  def name = column[String]("name", O.NotNull)

  def color = column[Option[String]]("color", O.NotNull)

  def rgb = column[Option[String]]("rgb", O.NotNull)

  def pdvCode = column[Option[Int]]("pdvcode", O.NotNull)

  def author = column[Option[String]]("author", O.NotNull)

  def createdAt = column[Option[DateTime]]("createdat", O.NotNull)

  def updatedAt = column[Option[DateTime]]("updatedat", O.NotNull)

  def * = (id, cupId, deviceCode, name, color, rgb, pdvCode, author, createdAt, updatedAt) <>(Booze.tupled, Booze.unapply)

}
