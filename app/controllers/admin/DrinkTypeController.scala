package controllers.admin

import java.util.UUID

import models.drinktype.Type
import play.api.libs.json.{JsError, Json}
import play.api.mvc.{Action, BodyParsers}
import services.TypeServiceComponent
import services.registry.DrinkTypeRegistry
import views.html

/**
 * Created by gbmetzner on 09/08/14.
 */
object DrinkTypeController extends DrinkTypeController with DrinkTypeRegistry

trait DrinkTypeController extends BaseController {
  this: TypeServiceComponent =>

  import utils.parsers.typeFormat

  def form = Action {
    Ok(html.admin.drinktype.add())
  }

  def save = Action(BodyParsers.parse.json) {
    implicit request =>

      request.body.validate[Type].fold(
        errors => BadRequest(Json.obj("status" -> "400", "message" -> JsError.toFlatJson(errors))),
        drinkType => {
          typeService persist drinkType
          Ok(Json.obj("status" -> "OK", "message" -> s"Drink Type: ${drinkType.description} saved."))
        }
      )
  }

  def update(id: UUID) = Action(BodyParsers.parse.json) {
    implicit request =>

      request.body.validate[Type].fold(
        errors => BadRequest(Json.obj("status" -> "400", "message" -> JsError.toFlatJson(errors))),
        drinkType => {
          typeService persist drinkType
          Ok(Json.obj("status" -> "OK", "message" -> s"Drink Type: ${drinkType.description} updated."))
        }
      )
  }

  /**
   * Find the drink's type
   *
   * @param id Current id of drink's type to be updated
   */
  def edit(id: UUID) = Action {
    implicit request =>
      typeService findById id match {
        case Some(drinkType) => Ok(html.admin.drinktype.edit(drinkType))
        case None => NotFound
      }
  }

  /**
   * Display the paginated list of computers.
   */
  def listPage = Action {
    implicit request =>
      Ok(html.admin.drinktype.list())
  }

  def pagination = Action {
    implicit request =>

      val (draw, start, length, query) = parseQueryStringPagination(request.queryString)

      val drinkTypes = typeService list(start, length, query)

      val drinkTypesJson = drinkTypes.items.map(dt => Json.toJson(dt))

      Ok(Json.obj("draw" -> draw, "recordsTotal" -> drinkTypes.total, "recordsFiltered" -> drinkTypes.total, "data" -> drinkTypesJson))
  }

  def delete(id: UUID) = Action {
    implicit request =>
      typeService delete id
      Ok(Json.obj("message" -> "Drink Type was deleted."))
  }

  def all = Action {
    Ok(Json.toJson(typeService listAll))
  }
}