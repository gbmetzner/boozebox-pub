package repositories.impl

import java.util.UUID

import models.booze.{Booze, BoozeTable}
import org.joda.time.DateTime
import repositories.BoozeRepositoryComponent
import utils.paginate.Page
import utils.uuid._

import scala.slick.driver.PostgresDriver.simple._

/**
 * Created by gbmetzner on 24/08/14.
 */
trait BoozeRepositoryComponentImpl extends BoozeRepositoryComponent {

  override def boozeRepository = new BoozeRepositoryImpl

  class BoozeRepositoryImpl extends BoozeRepository {

    private val tableQuery = TableQuery[BoozeTable]

    override def save(booze: Booze)(implicit session: Session): Unit = {
      val now = Some(DateTime.now)
      tableQuery += booze.copy(id = Some(uuid), createdAt = now, updatedAt = now)
    }

    override def update(booze: Booze)(implicit session: Session): Unit = {
      tableQuery.update(booze.copy(updatedAt = Some(DateTime.now)))
    }

    override def findById(id: UUID)(implicit session: Session): Option[Booze] = tableQuery.filter(_.id === id).firstOption

    override def delete(id: UUID)(implicit session: Session): Unit = tableQuery.filter(_.id === id).delete

    override def list(page: Int, length: Int, query: Option[String] = None)(implicit session: Session): Page[Booze] = {
      val pageSize = 10
      val offset = pageSize * page
      val cups = tableQuery.drop(offset).take(pageSize).list

      val total = tableQuery.length.run

      Page(cups, page, offset, total)
    }

    override def listAll()(implicit session: Session): List[Booze] = tableQuery.list.sortBy(_.deviceCode)

  }

}
