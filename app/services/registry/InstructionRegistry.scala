package services.registry

import repositories.impl.InstructionRepositoryComponentImpl
import services.impl.InstructionServiceComponentImpl

/**
 * Created by gbmetzner on 19/09/14.
 */
trait InstructionRegistry extends InstructionServiceComponentImpl with InstructionRepositoryComponentImpl
